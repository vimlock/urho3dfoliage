#include "Uniforms.glsl"
#include "Samplers.glsl"

varying vec2 vTexCoord;
varying vec4 vWorldPos;

#ifdef VERTEXCOLOR
    varying vec4 vColor;
#endif
#
#ifdef DISTANCEFADE
uniform float cFadeStart;
uniform float cFadeDistance;
#endif

void PS()
{
    // Get material diffuse albedo
    #ifdef DIFFMAP
        vec4 diffColor = cMatDiffColor * texture2D(sDiffMap, vTexCoord);

        #ifdef DISTANCEFADE
        diffColor.a *= clamp(((cFadeStart / cFarClipPS) - vWorldPos.w) * (cFarClipPS / cFadeDistance), 0.0, 1.0);
        #endif
        
        #ifdef ALPHAMASK
            if (diffColor.a < 0.4)
                discard;
        #endif
    #else
        vec4 diffColor = cMatDiffColor;
    #endif

    diffColor = vec4(1.0, 1.0, 1.0, 1.0);
    diffColor *=  vWorldPos.w * 10.0;
    
    #if defined(PREPASS)
        // Fill light pre-pass G-Buffer
        gl_FragData[0] = vec4(0.5, 0.5, 0.5, 1.0);
        gl_FragData[1] = vec4(EncodeDepth(vWorldPos.w), 0.0);
    #elif defined(DEFERRED)
        gl_FragData[0] = vec4(diffColor.rgb, 1.0);
        gl_FragData[1] = vec4(0.0, 0.0, 0.0, 0.0);
        gl_FragData[2] = vec4(0.5, 0.5, 0.5, 1.0);
        gl_FragData[3] = vec4(EncodeDepth(vWorldPos.w), 0.0);
    #else
        gl_FragColor = vec4(diffColor.rgb, 1.0);
    #endif
    
}
