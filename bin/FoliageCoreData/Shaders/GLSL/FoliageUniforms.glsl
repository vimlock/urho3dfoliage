
#if defined(FOLIAGE_NEAR_FADE) || defined(FOLIAGE_FAR_FADE)
    #define FOLIAGE_FADE

    // x and y are near fade parameters.
    // z and w are far fade parameters.
    uniform vec4 cFoliageFadeParams;
#endif

float GetFade(float dist, float near, float far)
{
    return clamp((far - dist) / (far - near), 0.0, 1.0);
}
