#pragma once

#include <Urho3D/Container/Vector.h>
#include <Urho3D/Container/Ptr.h>
#include <Urho3D/Core/Object.h>
#include <Urho3D/Math/Rect.h>

namespace Urho3D
{

class Node;
class Image;
class Material;
class Scene;
class Texture2D;
class BoundingBox;

class XMLFile;
class XMLElement;

namespace Foliage
{

/// Think of this class like an inverse cubemap, in which the
/// camera points towards the object from varying angles
class URHO3D_API BillboardMap : public RefCounted
{
public:
    friend class BillboardMapLoader;

    /// Get view which matches the given direction best.
    const Rect GetView(const Vector3& eyePosition, const Vector3& billboardPosition, float rotationOffset);
    /// Get view which matches the given direction best.
    const Rect GetView(float yaw, float pitch) const;

    float GetWidth() const { return width_; }
    float GetHeight() const { return height_; }
    float GetYOffset() const { return yOffset_; }

    /// Returns the material.
    Material* GetMaterial() const;

private:
    String name_;

    bool aboveOnly_;
    int yawViews_;
    int pitchViews_;

    float width_;
    float height_;

    float yOffset_;

    /// Array containing UV coordinates for all of the views
    /// size should be yawViews_ * pitchViews_.
    Vector<Rect> views_;

    /// Material to use for rendering.
    SharedPtr<Material> material_;

    /// Image containing the generated billboards.
    SharedPtr<Image> image_;
};

class URHO3D_API BillboardMapLoader : public Object
{
URHO3D_OBJECT(BillboardMapLoader, Object);

public:
    BillboardMapLoader(Context *context);

    static void RegisterSubsystem(Context *context);

    BillboardMap* Create(XMLFile *prefab, int yawViews, int pitchViews);

    /// Generate a new BillboardMap, yawViews and pitchViews
    /// must be 1 + power of two value, otherwise the billboards would be generated unenevenly.
    BillboardMap* Create(const String& name, const XMLElement& prefab,
        int yawViews, int pitchViews);

    void SetUseCache() { useCache_ = true; }
    bool GetUseCache() const { return useCache_; }

    void SetCacheDir(const String &value) { cacheDir_ = value; }

private:
    BillboardMap* GetCached(const String& name, int yawViews, int pitchViews) const;

    static String GetId(const String &name, int yawViews, int pitchViews);
    static bool GetBoundingBox(Node *node, BoundingBox *out);

    void OnUpdate(StringHash eventType, VariantMap &eventData);

    /// Are we currently waiting for a frame to finish?
    bool renderPending_;

    /// Should the the generated billboards be cached?
    bool useCache_;

    /// Which directory to use to store generated billboard images.
    String cacheDir_;

    /// Resolution of the generated images. Should be power of two.
    int imageResolution_;

    /// Scene we use for rendering
    SharedPtr<Scene> scene_;
    SharedPtr<Texture2D> renderTexture_;

    /// Currently rendered map
    SharedPtr<BillboardMap> renderMap_;

    /// RenderPath to use for rendering the scene
    SharedPtr<XMLFile> renderPath_;

    /// Material to use for the billboards.
    /// Separate copy is made for each generated billboard map.
    SharedPtr<Material> material_;
};

} // Foliage

} // Urho3D
