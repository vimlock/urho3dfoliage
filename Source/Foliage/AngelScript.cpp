#include "Foliage/AngelScript.h"

#include "Foliage/PatchManager.h"
#include "Foliage/PatchLoader.h"
#include "Foliage/Loaders/TreeLoader.h"
#include "Foliage/Loaders/GrassLoader.h"
#include "Foliage/BillboardMap.h"

namespace Urho3D
{

namespace Foliage
{

void RegisterPatchManager(asIScriptEngine* engine)
{
    RegisterComponent<PatchManager>(engine, "PatchManager");
}

void RegisterPatchLoader(asIScriptEngine* engine)
{
    RegisterPatchLoader<PatchLoader>(engine, "PatchLoader");
    // RegisterSubclass<Component, PatchLoader>(engine, "Component", "PatchLoader");
}

void RegisterGrassLoader(asIScriptEngine* engine)
{
    RegisterComponent<GrassLoader>(engine, "GrassLoader");

    engine->RegisterObjectMethod("GrassLoader", "void MarkDirty()", asMETHOD(GrassLoader, MarkDirty), asCALL_THISCALL);

    engine->RegisterObjectMethod("GrassLoader", "void set_densityMap(Image@+)", asMETHOD(GrassLoader, SetDensityMap), asCALL_THISCALL);
    engine->RegisterObjectMethod("GrassLoader", "Image@+ get_densityMap() const", asMETHOD(GrassLoader, GetDensityMap), asCALL_THISCALL);
}

void RegisterTreeLoader(asIScriptEngine* engine)
{
    RegisterComponent<TreeLoader>(engine, "TreeLoader");

    engine->RegisterObjectMethod("TreeLoader", "void MarkDirty()", asMETHOD(TreeLoader, MarkDirty), asCALL_THISCALL);
    engine->RegisterObjectMethod("TreeLoader", "bool AddInstance(const Vector2&in, float, Vector2&in)", asMETHOD(TreeLoader, AddInstanceHelper), asCALL_THISCALL);
    engine->RegisterObjectMethod("TreeLoader", "void RemoveInstance(uint)", asMETHOD(TreeLoader, RemoveInstance), asCALL_THISCALL);
    engine->RegisterObjectMethod("TreeLoader", "void RemoveInstances()", asMETHOD(TreeLoader, RemoveInstances), asCALL_THISCALL);

    engine->RegisterObjectMethod("TreeLoader", "bool get_createComponents() const", asMETHOD(TreeLoader, GetCreateComponents), asCALL_THISCALL);
    engine->RegisterObjectMethod("TreeLoader", "void set_createComponents(bool)", asMETHOD(TreeLoader, SetCreateComponents), asCALL_THISCALL);
    engine->RegisterObjectMethod("TreeLoader", "float get_billboardDistance() const", asMETHOD(TreeLoader, GetBillboardDistance), asCALL_THISCALL);
    engine->RegisterObjectMethod("TreeLoader", "void set_billboardDistance(float)", asMETHOD(TreeLoader, SetBillboardDistance), asCALL_THISCALL);
}

void RegisterBillboardMap(asIScriptEngine* engine)
{
    RegisterRefCounted<BillboardMap>(engine, "BillboardMap");
}

/// Register the Foliage library to script.
void RegisterFoliageAPI(asIScriptEngine* engine)
{
    RegisterPatchManager(engine);
    RegisterPatchLoader(engine);
    RegisterGrassLoader(engine);
    RegisterTreeLoader(engine);
    RegisterBillboardMap(engine);
}

}

}
