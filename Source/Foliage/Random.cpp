#include "Random.h"

namespace Urho3D
{

namespace Foliage
{

Random::Random(int seed):
    seed_(seed)
{
}

int Random::GetSeed() const
{
    return seed_;
}

void Random::SetSeed(int seed)
{
    seed_ = seed;
}

int Random::NextInt()
{
    seed_ = seed_ * 214013 + 2531011;
    return (seed_ >> 16) & 32767;
}

float Random::NextFloat()
{
    return NextInt() / 32767.0f;
}

float Random::NextFloat(float range)
{
    return NextInt() * range / 32767.0f;
}

float Random::NextFloat(float min, float max)
{
    return NextInt() * (max - min) / 32767.0f + min;
}

}

}

