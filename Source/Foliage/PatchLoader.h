#pragma once

#include <Urho3D/Scene/Component.h>
#include <Urho3D/Core/Object.h>

namespace Urho3D
{

class Drawable;

namespace Foliage
{

class Patch;
class PatchManager;

class URHO3D_API PatchLoader : public Component
{
URHO3D_OBJECT(PatchLoader, Component);

public:
    friend class PatchManager;

    /// Construct
    PatchLoader(Context *context);

    /// Destruct
    ~PatchLoader();

    static void RegisterObject(Context *context);

    /// Called every frame
    virtual void Update(PatchManager *pm, float timeStep) { }

    /// Called when PatchManager wants us to generate a new patch
    virtual void Load(Patch *patch) { }
    
    virtual void OnSetAttribute(const AttributeInfo& attr, const Variant& src);

    /// Called when a Patch is no longer needed
    /// It is up to the implementor to define what this function should do.
    /// Possible use case for this would be to serialize the patch data back to disk
    virtual void Unload(Patch *patch) { }

    /// Called when the PatchLoader is registed by the PatchManager
    void SetPatchManager(PatchManager *patchManager);

    bool IsDirty() { return dirty_; }

    /// Set view mask. Is and'ed with camera's view mask to see if the object should be rendered.
    void SetViewMask(unsigned mask) { viewMask_ = mask; }
    /// Set light mask. Is and'ed with light's and zone's light mask to see if the object should be lit.
    void SetLightMask(unsigned mask) { lightMask_ = mask; }
    /// Set shadow mask. Is and'ed with light's light mask and zone's shadow mask to see if the object should be rendered to a shadow map.
    void SetShadowMask(unsigned mask) { shadowMask_ = mask; }
    /// Set zone mask. Is and'ed with zone's zone mask to see if the object should belong to the zone.
    void SetZoneMask(unsigned mask) { zoneMask_ = mask; }
    /// Set maximum number of per-pixel lights. Default 0 is unlimited.
    void SetMaxLights(unsigned num) { maxLights_ = num; }
    /// Set shadowcaster flag.
    void SetCastShadows(bool enable) { castShadows_ = enable; }

    /// Return cast shadows flag.
    bool GetCastShadows() const { return castShadows_; }

    /// Return view mask.
    unsigned GetViewMask() const { return viewMask_; }

    /// Return light mask.
    unsigned GetLightMask() const { return lightMask_; }

    /// Return shadow mask.
    unsigned GetShadowMask() const { return shadowMask_; }

    /// Return zone mask.
    unsigned GetZoneMask() const { return zoneMask_; }

    /// Return maximum number of per-pixel lights.
    unsigned GetMaxLights() const { return maxLights_; }

    PatchManager* GetPatchManager() const { return patchManager_; }

    /// Inform the PatchManager that Patches should be reloaded
    void MarkDirty() { dirty_ = true; }
    void ClearDirty() { dirty_ = false; }

protected:

    /// Called when this patch loader has been registered to a PatchManager.
    virtual void OnRegistered(PatchManager *pm) { }
    /// Called before the first update. 
    virtual void OnStart();
    /// Called on scene update, variable timestep.
    virtual void OnUpdate(StringHash eventType, VariantMap &param);
    /// Handle scene node being assigned at creation or deletion.
    virtual void OnNodeSet(Node* node);
    /// Initializes a drawable from standard rendering attributes (castShadows, viewMask, etc.).
    void AssignDrawableAttributes(Drawable *drawable) const;

private:
    /// PatchManager to which this PatchLoader is assigned to.
    WeakPtr<PatchManager> patchManager_;

    /// Shadowcaster flag.
    bool castShadows_;
    /// View mask.
    unsigned viewMask_;
    /// Light mask.
    unsigned lightMask_;
    /// Shadow mask.
    unsigned shadowMask_;
    /// Zone mask.
    unsigned zoneMask_;
    /// Maximum per-pixel lights.
    unsigned maxLights_;
    
    bool dirty_;
    bool startCalled_;
};


} // Foliage

} // Urho3D
