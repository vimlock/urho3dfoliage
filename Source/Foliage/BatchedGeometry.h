#pragma once

#include "Urho3D/Graphics/Drawable.h"

namespace Urho3D
{

class VertexBuffer;
class IndexBuffer;
class Material;
class Model;

namespace Foliage
{

struct BatchInstanceDesc
{
    Vector3 position_;
    Quaternion rotation_;
    Vector3 scale_;
    Color color_;
};

class URHO3D_API BatchedGeometry : public Drawable
{
URHO3D_OBJECT(BatchedGeometry, Drawable);

public:
    BatchedGeometry(Context *context);

    static void RegisterObject(Context* context);

    virtual void UpdateBatches(const FrameInfo& frame);

    /// Process octree raycast. May be called from a worker thread.
    virtual void ProcessRayQuery(const RayOctreeQuery& query, PODVector<RayQueryResult>& results);
    /// Return the geometry for a specific LOD level.
    virtual Geometry* GetLodGeometry(unsigned batchIndex, unsigned level);
    /// Return number of occlusion geometry triangles.
    virtual unsigned GetNumOccluderTriangles();
    /// Draw to occlusion buffer. Return true if did not run out of triangles.
    virtual bool DrawOcclusion(OcclusionBuffer* buffer);

    bool Create(Model *model, Material* material, const PODVector<BatchInstanceDesc>& instances);

protected:
    virtual void OnWorldBoundingBoxUpdate();

    SharedPtr<Geometry> CreateGeometry(
        const PODVector<BatchInstanceDesc>& instances,
        unsigned startInstance, unsigned count
    );

private:
    Vector<SharedPtr<Geometry> > geometries_;
    SharedPtr<Model> model_;
    SharedPtr<Material> material_;

};

}

}
