#include "PatchLoader.h"
#include "PatchManager.h"

#include <Urho3D/Core/Context.h>
#include <Urho3D/Graphics/Drawable.h>
#include <Urho3D/Scene/SceneEvents.h>

namespace Urho3D
{

namespace Foliage
{

PatchLoader::PatchLoader(Context *context):
    Component(context),

    castShadows_(false),
    viewMask_(DEFAULT_VIEWMASK),
    lightMask_(DEFAULT_LIGHTMASK),
    shadowMask_(DEFAULT_SHADOWMASK),
    zoneMask_(DEFAULT_ZONEMASK),
    maxLights_(0),

    dirty_(true),
    startCalled_(false)
{
    SubscribeToEvent(E_SCENEUPDATE, URHO3D_HANDLER(PatchLoader, OnUpdate));
}

PatchLoader::~PatchLoader()
{
}

void PatchLoader::RegisterObject(Context *context)
{
    URHO3D_ATTRIBUTE("Cast Shadows", bool, castShadows_, false, AM_DEFAULT);
    URHO3D_ATTRIBUTE("Max Lights", int, maxLights_, 0, AM_DEFAULT);
    URHO3D_ATTRIBUTE("View Mask", int, viewMask_, DEFAULT_VIEWMASK, AM_DEFAULT);
    URHO3D_ATTRIBUTE("Light Mask", int, lightMask_, DEFAULT_LIGHTMASK, AM_DEFAULT);
    URHO3D_ATTRIBUTE("Shadow Mask", int, shadowMask_, DEFAULT_SHADOWMASK, AM_DEFAULT);
    URHO3D_ATTRIBUTE("Zone Mask", unsigned, zoneMask_, DEFAULT_ZONEMASK, AM_DEFAULT);
}

void PatchLoader::OnSetAttribute(const AttributeInfo& attr, const Variant& src)
{
    Serializable::OnSetAttribute(attr, src);

    MarkDirty();
}

void PatchLoader::SetPatchManager(PatchManager *patchManager)
{
    patchManager_ = patchManager;
    OnRegistered(patchManager);
}

void PatchLoader::OnStart()
{
    PatchManager *pm = GetComponent<PatchManager>();
    if (pm)
    {
        pm->AddLoader(this);
        OnRegistered(pm);
    }
}

void PatchLoader::OnUpdate(StringHash eventType, VariantMap &param)
{
    if (!startCalled_)
    {
        OnStart();
        startCalled_ = true;
    }
}

void PatchLoader::OnNodeSet(Node* node)
{
    // We only care about when the case when the node is removed.
    if (node)
        return;

    if (patchManager_)
        patchManager_->RemoveLoader(this);

}

void PatchLoader::AssignDrawableAttributes(Drawable *drawable) const
{
    drawable->SetCastShadows(castShadows_);
    drawable->SetViewMask(viewMask_);
    drawable->SetLightMask(lightMask_);
    drawable->SetShadowMask(shadowMask_);
    drawable->SetMaxLights(maxLights_);
}

} // Foliage

} // Urho3D
