#pragma once

#include <Urho3D/Container/Ptr.h>
#include <Urho3D/Urho3D.h>

namespace Urho3D
{

namespace Foliage
{

class Patch;
class PatchLoader;

class URHO3D_API PatchLayer : public RefCounted
{
public:
    /// Construct.
    PatchLayer();

    /// Destruct.
    virtual ~PatchLayer();
    
    virtual PatchLoader* GetLoader() const = 0;
    virtual void UpdateLodDistance(float newDistance, Patch* patch) = 0;

private:
};

} // Foliage

} // Urho3D
