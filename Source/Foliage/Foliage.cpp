#include "Foliage.h"

#include "Foliage/BatchedGeometry.h"
#include "Foliage/BillboardMap.h"

#include "Foliage/Loaders/GrassLoader.h"
#include "Foliage/Loaders/TreeLoader.h"

#include "Foliage/Patch.h"
#include "Foliage/PatchManager.h"

#ifdef URHO3D_ANGELSCRIPT
#include "Foliage/AngelScript.h"
#endif

#ifdef URHO3D_LUA
// #include "Foliage/LuaScript/LuaScript.h"
#endif

namespace Urho3D
{

namespace Foliage
{

const char* FOLIAGE_CATEGORY = "Foliage";

void URHO3D_API RegisterFoliageLibrary(Context *context)
{
    BillboardMapLoader::RegisterSubsystem(context);
    BatchedGeometry::RegisterObject(context);

    Patch::RegisterObject(context);
    PatchManager::RegisterObject(context);

    PatchLoader::RegisterObject(context);
    GrassLoader::RegisterObject(context);
    TreeLoader::RegisterObject(context);

}

} // Foliage

} // Urho3D
