#pragma once

#include <Urho3D/Urho3D.h>

namespace Urho3D
{

class Context;

namespace Foliage
{

extern const char *FOLIAGE_CATEGORY;

void URHO3D_API RegisterFoliageLibrary(Context *context);

} // Foliage

} // Urho3D
