#include "PatchManager.h"
#include "PatchLoader.h"
#include "Foliage.h"
#include "Patch.h"

#include <Urho3D/Core/Context.h>
#include <Urho3D/Core/CoreEvents.h>
#include <Urho3D/Core/Profiler.h>
#include <Urho3D/Core/Timer.h>

#include <Urho3D/Graphics/DrawableEvents.h>
#include <Urho3D/Graphics/DebugRenderer.h>
#include <Urho3D/Graphics/Camera.h>
#include <Urho3D/Graphics/Renderer.h>
#include <Urho3D/Graphics/Terrain.h>

#include <Urho3D/Resource/Image.h>

#include <Urho3D/IO/Log.h>

#include <Urho3D/Scene/Scene.h>
#include <Urho3D/Scene/SceneEvents.h>

namespace Urho3D
{

namespace Foliage
{

PatchManager::PatchManager(Context *context):
    Component(context),

    loadDistance_(30.0f),
    cacheDistance_(200.0f),

    patchSubdivisons_(2),

    maxLoadTime_(5),
    updateInterval_(0.5f),

    patchesSize_(IntVector2::ZERO),
    patchWorldSize_(Vector2::ZERO),

    viewPos_(Vector3::ZERO),

    updateTimer_(0.0f),
    startCalled_(false),
    recreate_(false)
{
    // Do updates in post update because PatchLoaders register and update themselves during E_SCENEUPDATE.
    // If we were to update ourselves between PatchLoader updates, that would cause us to perform
    // unnecessary updates to patches because they would be just be dirtied the next frame.
    SubscribeToEvent(E_SCENEPOSTUPDATE, URHO3D_HANDLER(PatchManager, OnUpdate));
}

PatchManager::~PatchManager()
{
    RemovePatches();

    while (loaders_.Size())
        RemoveLoader(loaders_[0]);
}

void PatchManager::RegisterObject(Context *context)
{
    context->RegisterFactory<PatchManager>(FOLIAGE_CATEGORY);

    URHO3D_ATTRIBUTE("Max Load Time", unsigned, maxLoadTime_, 5, AM_DEFAULT);
    URHO3D_ATTRIBUTE("Update Interval", float, updateInterval_, 0.5f, AM_DEFAULT);

    URHO3D_ATTRIBUTE("Load Distance", float, loadDistance_, 300.0f, AM_DEFAULT);
    URHO3D_ATTRIBUTE("Cache Distance", float, cacheDistance_, 500.0f, AM_DEFAULT);

    URHO3D_ACCESSOR_ATTRIBUTE("Patch Subdivisions", GetPatchSubdivisions, SetPatchSubvisions, int, 2, AM_DEFAULT);
}

void PatchManager::OnSetAttribute(const AttributeInfo& attr, const Variant& src)
{
    Serializable::OnSetAttribute(attr, src);
    recreate_ = true;
}

void PatchManager::AddLoader(PatchLoader *loader)
{
    if (!loader)
        return;

    loaders_.Push(SharedPtr<PatchLoader>(loader));
    loader->SetPatchManager(this);

    URHO3D_LOGINFO("Registered loader");
}

PatchLoader* PatchManager::GetLoader(unsigned index)
{
    if (index < loaders_.Size())
        return 0;

    return loaders_[index];
}

void PatchManager::RemoveLoader(PatchLoader *loader)
{
    loaders_.Remove(SharedPtr<PatchLoader>(loader));

    for (unsigned i = 0; i < patches_.Size(); ++i)
    {
        Patch *patch = patches_[i];
        if (!patch)
            continue;

        patch->MarkDirty();
    }
}

void PatchManager::RemoveLoader(unsigned index)
{
    RemoveLoader(GetLoader(index));
}

void PatchManager::Start()
{
    if (!terrain_)
        SetTerrain(GetComponent<Terrain>());

    if (!camera_)
    {
        Renderer *renderer = GetSubsystem<Renderer>();
        if (!renderer)
        {
            URHO3D_LOGERROR("Can't initialize PatchManager without graphics");
            return;
        }

        Viewport* viewport = renderer->GetViewport(0);
        if (!viewport)
        {
            URHO3D_LOGERROR("Can't initialize PatchManager: default viewport is missing");
            return;
        }

        Camera *cam = viewport->GetCamera();
        if (!cam)
        {
            URHO3D_LOGERROR("Can't initialize PatchManager: default viewport does not have camera");
            return;
        }

        SetCamera(cam->GetNode());
    }

    updateTimer_ = 0.0f;
}

void PatchManager::OnUpdate(StringHash eventType, VariantMap& eventData)
{
    using namespace ScenePostUpdate;

    if (!startCalled_)
    {
        Start();
        startCalled_ = true;
    }

    if (!terrain_)
        return;

    URHO3D_PROFILE(UpdateFoliage);

    float timeStep = eventData[P_TIMESTEP].GetFloat();

    if (camera_)
        viewPos_ = camera_->GetWorldPosition();

    bool loadersDirty = false;

    for (unsigned i = 0; i < loaders_.Size(); ++i)
    {
        PatchLoader *loader = loaders_[i];
        if (loader)
        {
            loader->Update(this, timeStep);

            if (loader->IsDirty())
            {
                loader->ClearDirty();
                loadersDirty = true;
            }
        }
    }

    if (recreate_ || loadersDirty)
        ReloadAllPatches();

    if (updateTimer_ > 0.0f)
        updateTimer_ -= timeStep;

    if (updateTimer_ > 0.0f)
        return;

    LoadArea(viewPos_, loadDistance_);

    unsigned numLoaded = 0;
    unsigned startTime = Time::GetSystemTime();
    for (unsigned i = 0; i < loadedPatches_.Size(); ++i)
    {
        Patch *patch = loadedPatches_[i];
        if (!patch)
            continue;

        if (patch->IsDirty())
        {
            unsigned td = Time::GetSystemTime() - startTime;
            if (td < maxLoadTime_)
            {
                numLoaded++;
                ReloadPatch(patch);
            }
        }

        float dist = (patch->GetNode()->GetWorldPosition() - viewPos_).Length();
        patch->SetLodDistance(dist);
        patch->Update(1.0f);
    }

    if (numLoaded != 0)
        updateTimer_ = updateInterval_;
}

void PatchManager::DrawDebugGeometry(DebugRenderer* debug, bool depthTest)
{
    for (unsigned i = 0; i < loadedPatches_.Size(); ++i)
    {
        Patch *patch = loadedPatches_[i];
        if (!patch)
            continue;

        patch->DrawDebugGeometry(debug, false);
    }
}

void PatchManager::DrawDebugGeometry(bool depthTest)
{
    Scene *scene = GetScene();
    if (scene)
    {
        DebugRenderer *debug = scene->GetComponent<DebugRenderer>();
        if (!debug || !debug->IsEnabledEffective())
            return;

        DrawDebugGeometry(debug, depthTest);
    }
}

void PatchManager::OnTerrainCreated(StringHash eventType, VariantMap& eventData)
{
    recreate_ = true;
}

Vector2 PatchManager::GetSize() const
{
    return Vector2(
        patchesSize_.x_ * patchWorldSize_.x_,
        patchesSize_.y_ * patchWorldSize_.y_
    );
}

Rect PatchManager::GetBounds() const
{
    Vector2 half(
        0.5f * patchesSize_.x_ * patchWorldSize_.x_,
        0.5f * patchesSize_.y_ * patchWorldSize_.y_
    );

    return Rect(-half, half);
}

void PatchManager::LoadArea(const Vector3 &point, float distance)
{
    Vector2 offset = Vector2(
        point.x_ - (-0.5f * (float)patchesSize_.x_ * patchWorldSize_.x_),
        point.z_ - (-0.5f * (float)patchesSize_.y_ * patchWorldSize_.y_)
    );

    LoadArea(
        Rect(
            offset - Vector2(distance, distance),
            offset + Vector2(distance, distance)
        )
    );
}

void PatchManager::LoadArea(const Rect &area)
{
    int xMin = Floor(area.min_.x_) / patchWorldSize_.x_;
    int zMin = Floor(area.min_.y_) / patchWorldSize_.y_;
    int xMax = Floor(area.max_.x_) / patchWorldSize_.x_;
    int zMax = Floor(area.max_.y_) / patchWorldSize_.y_;

    LoadPatchRegion(IntRect(xMin, zMin, xMax, zMax));
}

void PatchManager::LoadPatchRegion(const IntRect &area)
{
    Scene *scene = GetScene();
    if (scene == NULL)
    {
        URHO3D_LOGWARNING("FoliageRender not set to scene");
        return;
    }

    for (int z = area.top_; z <= area.bottom_; z++)
    {
        for (int x = area.left_; x <= area.right_; ++x)
        {
            Patch *patch = GetPatch(x, z);
            if (patch)
                patch->MarkActive();
            else
                LoadPatch(scene, x, z);
        }
    }
}

void PatchManager::ReloadPatch(Patch* patch)
{
    patch->ClearLayers();
    patch->ClearDirty();

    patch->MarkActive();

    // Remove all components and children from the patch except the Patch component
    // because we will be reusing the same scene node
    
    Node *node = patch->GetNode();
    node->RemoveAllChildren();

    unsigned i = 0;
    while (i < node->GetNumComponents())
    {
        const Vector<SharedPtr<Component> >& components = node->GetComponents();

        // Careful not to remove the Patch component we use to keep track of the
        // patch node.
        if (components[i] != patch)
            node->RemoveComponent(components[i]);
        else
            i++;
    }

    for(unsigned i = 0; i < loaders_.Size(); ++i)
    {
        PatchLoader *loader = loaders_[i];
        if (loader)
            loader->Load(patch);
    }
}

void PatchManager::ReloadAllPatches()
{
    if (recreate_)
    {
        recreate_ = false;

        RemovePatches();
        SetTerrain(terrain_);
        return;
    }

    for (unsigned i = 0; i < loadedPatches_.Size(); ++i)
    {
        Patch *p = loadedPatches_[i];
        if (p == NULL)
            continue;

        p->MarkDirty();
    }
}

void PatchManager::RemovePatch(Patch *patch)
{
    if (!patch)
        return;

    for (unsigned i = 0; i < loaders_.Size(); ++i)
    {
        PatchLoader* loader = loaders_[i];
        if (loader)
            loader->Unload(patch);
    }

    patch->GetNode()->Remove();
    loadedPatches_.RemoveSwap(SharedPtr<Patch>(patch));
}

void PatchManager::RemovePatches()
{
    for (unsigned i = 0; i < patches_.Size(); ++i)
    {
        Patch* p = patches_[i];
        if (!p)
            continue;

        // Because we maintain a shared ptr to the patch, the node could have been
        // removed by this point 
        Node* n = p->GetNode();
        if (!n)
            continue;

        n->Remove();
    }

    patches_.Clear();
    loadedPatches_.Clear();
}

void PatchManager::LoadPatch(Scene *scene, int x, int z)
{
    if (x < 0 || z < 0)
        return;

    if (x >= patchesSize_.x_ || z >= patchesSize_.y_)
        return;

    Vector2 offset = GetSize() * -0.5f;
    float xHalf = patchWorldSize_.x_ * 0.5f;
    float zHalf = patchWorldSize_.y_ * 0.5f;

    String name = "Foliage " + String(x) + "_" + String(z);
    Vector3 point = Vector3(
        offset.x_ + x * patchWorldSize_.x_ + xHalf,
        0.0f,
        offset.y_ + z * patchWorldSize_.y_ + zHalf
    );

    Node* node(scene->CreateTemporaryChild(name));
    node->SetPosition(point);

    SharedPtr<Patch> patch(node->CreateComponent<Patch>());

    patch->SetOwner(this);

    patch->SetBounds(Rect(
        Vector2(point.x_ - xHalf, point.z_ - zHalf),
        Vector2(point.x_ + xHalf, point.z_ + zHalf)
    ));

    patch->SetIndex(IntVector2(x, z));
    patch->SetSeed(z * patchesSize_.y_ + x);
    patch->MarkDirty();
    
    patches_[z * patchesSize_.y_ + x] = patch;
    loadedPatches_.Push(patch);
}

Patch* PatchManager::GetPatch(int x, int z)
{
    if (x < 0 || z < 0)
        return 0;

    if (x >= patchesSize_.x_ || z >= patchesSize_.y_)
        return 0;

    return patches_[z * patchesSize_.y_ + x];
}

void PatchManager::SetPatchSubvisions(int value)
{
    if (value < 0)
        value = 0;

    patchSubdivisons_ = value;
}

void PatchManager::SetTerrain(Terrain *terrain)
{
    RemovePatches();

    if (terrain_)
    {
        UnsubscribeFromEvent(terrain_->GetNode(), E_TERRAINCREATED);
    }

    terrain_ = terrain;

    if (!terrain_)
        return;

    SubscribeToEvent(terrain_->GetNode(), E_TERRAINCREATED, URHO3D_HANDLER(PatchManager, OnTerrainCreated));

    Image* heightMap = terrain_->GetHeightMap();
    if (heightMap == NULL)
    {
        URHO3D_LOGWARNING("Heightmap missing from Terrain");
        return;
    }

    Vector3 tmp = terrain_->GetSpacing();
    int patchSize = terrain_->GetPatchSize() / (patchSubdivisons_ + 1);

    Vector2 spacing = Vector2(tmp.x_, tmp.z_);

    patchWorldSize_ = Vector2(
        spacing.x_ * patchSize, 
        spacing.y_ * patchSize
    );

    patchesSize_ = IntVector2(
        (heightMap->GetWidth() - 1) / patchSize,
        (heightMap->GetHeight() - 1) / patchSize
    );

    patches_.Resize(patchesSize_.x_ * patchesSize_.y_);
}

void PatchManager::SetCamera(Node *value)
{
    if (!value)
    {
        URHO3D_LOGWARNING("Setting FoliageRender camera to NULL");
        return;
    }

    URHO3D_LOGINFOF("Set camera to %u", value->GetID());

    camera_ = value;
}

void PatchManager::SetCameraNodeIdAttr(unsigned value)
{
    Scene* scene = GetScene();

    SetCamera(scene->GetNode(value));
}

unsigned PatchManager::GetCameraNodeIdAttr() const
{
    if (camera_)
        return camera_->GetID();
    else
        return 0;
}

} // Foliage

} // Urho3D
