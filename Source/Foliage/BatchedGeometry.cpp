#include "BatchedGeometry.h"

#include <Urho3D/Core/Context.h>

#include <Urho3D/Graphics/Camera.h>
#include <Urho3D/Graphics/Material.h>
#include <Urho3D/Graphics/Geometry.h>
#include <Urho3D/Graphics/Model.h>
#include <Urho3D/Graphics/VertexBuffer.h>
#include <Urho3D/Graphics/IndexBuffer.h>
#include <Urho3D/Graphics/StaticModel.h>

#include <Urho3D/Graphics/OcclusionBuffer.h>
#include <Urho3D/Graphics/OctreeQuery.h>

#include <Urho3D/Math/Matrix3x4.h>

#include <Urho3D/IO/Log.h>

#include <limits.h>

namespace Urho3D
{

namespace Foliage
{

static int CeilDiv(unsigned x, unsigned y)
{
    return (x % y) ? x / y + 1 : x / y;
}

BatchedGeometry::BatchedGeometry(Context *context):
    Drawable(context, DRAWABLE_GEOMETRY)
{
}

void BatchedGeometry::RegisterObject(Context* context)
{
    context->RegisterFactory<BatchedGeometry>();

    URHO3D_ACCESSOR_ATTRIBUTE("Is Enabled", IsEnabled, SetEnabled, bool, true, AM_DEFAULT);
    URHO3D_ACCESSOR_ATTRIBUTE("Draw Distance", GetDrawDistance, SetDrawDistance, float, 0.0f, AM_DEFAULT);
    URHO3D_ATTRIBUTE("Is Occluder", bool, occluder_, false, AM_DEFAULT);
    URHO3D_ACCESSOR_ATTRIBUTE("Can Be Occluded", IsOccludee, SetOccludee, bool, true, AM_DEFAULT);
    URHO3D_ATTRIBUTE("Cast Shadows", bool, castShadows_, false, AM_DEFAULT);

    URHO3D_COPY_BASE_ATTRIBUTES(Drawable);
}

void BatchedGeometry::UpdateBatches(const FrameInfo& frame)
{
    const BoundingBox& worldBoundingBox = GetWorldBoundingBox();
    const Matrix3x4& worldTransform = node_->GetWorldTransform();
    distance_ = frame.camera_->GetDistance(worldBoundingBox.Center());

    if (batches_.Size() == 1)
    {
        batches_[0].distance_ = distance_;
        batches_[0].worldTransform_ = &worldTransform;
    }
    else
    {
        const Matrix3x4& worldTransform = node_->GetWorldTransform();
        for (unsigned i = 0; i < batches_.Size(); ++i)
            batches_[i].distance_ = frame.camera_->GetDistance(worldTransform * Vector3::ZERO);
    }
}

void BatchedGeometry::ProcessRayQuery(const RayOctreeQuery& query, PODVector<RayQueryResult>& results)
{
    RayQueryLevel level = query.level_;

    switch (level)
    {
    case RAY_AABB:
        Drawable::ProcessRayQuery(query, results);
        break;

    case RAY_OBB:
    case RAY_TRIANGLE:
    case RAY_TRIANGLE_UV:
        /*
        Matrix3x4 inverse(node_->GetWorldTransform().Inverse());
        Ray localRay = query.ray_.Transformed(inverse);
        float distance = localRay.HitDistance(boundingBox_);
        Vector3 normal = -query.ray_.direction_;
        Vector2 geometryUV;
        unsigned hitBatch = M_MAX_UNSIGNED;

        if (level >= RAY_TRIANGLE && distance < query.maxDistance_)
        {
            distance = M_INFINITY;

            for (unsigned i = 0; i < batches_.Size(); ++i)
            {
                Geometry* geometry = batches_[i].geometry_;
                if (geometry)
                {
                    Vector3 geometryNormal;
                    float geometryDistance = level == RAY_TRIANGLE ? geometry->GetHitDistance(localRay, &geometryNormal) :
                        geometry->GetHitDistance(localRay, &geometryNormal, &geometryUV);
                    if (geometryDistance < query.maxDistance_ && geometryDistance < distance)
                    {
                        distance = geometryDistance;
                        normal = (node_->GetWorldTransform() * Vector4(geometryNormal, 0.0f)).Normalized();
                        hitBatch = i;
                    }
                }
            }
        }

        if (distance < query.maxDistance_)
        {
            RayQueryResult result;
            result.position_ = query.ray_.origin_ + distance * query.ray_.direction_;
            result.normal_ = normal;
            result.textureUV_ = geometryUV;
            result.distance_ = distance;
            result.drawable_ = this;
            result.node_ = node_;
            result.subObject_ = hitBatch;
            results.Push(result);
        }
        */
        break;
    }
}

Geometry* BatchedGeometry::GetLodGeometry(unsigned batchIndex, unsigned level)
{
    if (batchIndex < geometries_.Size())
        return geometries_[batchIndex];
    else
        return 0;
}

unsigned BatchedGeometry::GetNumOccluderTriangles()
{
    unsigned triangles = 0;

    Geometry* geometry = GetLodGeometry(0, 0);
    if (!geometry)
        return 0;

    // Check that the material is suitable for occlusion (default material always is)
    Material* mat = batches_[0].material_;
    if (mat && !mat->GetOcclusion())
        return 0;

    triangles += geometry->GetVertexCount() / 3;

    return triangles;
}

bool BatchedGeometry::DrawOcclusion(OcclusionBuffer* buffer)
{
    for (unsigned i = 0; i < batches_.Size(); ++i)
    {
        Geometry* geometry = GetLodGeometry(i, 0);
        if (!geometry)
            continue;

        // Check that the material is suitable for occlusion (default material always is) and set culling mode
        Material* material = batches_[i].material_;
        if (material)
        {
            if (!material->GetOcclusion())
                continue;
            buffer->SetCullMode(material->GetCullMode());
        }
        else
            buffer->SetCullMode(CULL_CCW);

        const unsigned char* vertexData;
        unsigned vertexSize;
        const unsigned char* indexData;
        unsigned indexSize;
        const PODVector<VertexElement>* elements;

        geometry->GetRawData(vertexData, vertexSize, indexData, indexSize, elements);
        // Check for valid geometry data
        if (!vertexData || !indexData || !elements || VertexBuffer::GetElementOffset(*elements, TYPE_VECTOR3, SEM_POSITION) != 0)
            continue;

        unsigned indexStart = geometry->GetIndexStart();
        unsigned indexCount = geometry->GetIndexCount();

        // Draw and check for running out of triangles
        if (!buffer->AddTriangles(node_->GetWorldTransform(), vertexData, vertexSize, indexData, indexSize, indexStart, indexCount))
            return false;
    }

    return true;
}

bool BatchedGeometry::Create(Model *model, Material *material, const PODVector<BatchInstanceDesc>& instances)
{
    if (!model)
    {
        URHO3D_LOGERROR("Cant create BatchedGeometry without model");
        return false;
    }

    if (!material)
    {
        URHO3D_LOGERROR("Can't create BatchedGeometry without material");
        return false;
    }

    if (instances.Size() <= 0)
        return false;

    model_ = model;
    material_ = material;

    boundingBox_ = BoundingBox(0.0f, 0.0f);

    VertexBuffer* origBuffer = model_->GetGeometry(0, 0)->GetVertexBuffer(0);
    IndexBuffer* origIndex = model_->GetGeometry(0, 0)->GetIndexBuffer();

    unsigned vertexCount = origBuffer->GetVertexCount();
    unsigned instanceSize = vertexCount;

    origBuffer->Unlock();
    origIndex->Unlock();

    unsigned numGeoms = CeilDiv(instances.Size() * instanceSize, USHRT_MAX);
    unsigned instancesPerGeom = instances.Size() / numGeoms;

    batches_.Resize(numGeoms);
    geometries_.Resize(numGeoms);

    const Matrix3x4* worldTransform = node_ ? &node_->GetWorldTransform() : (const Matrix3x4*)0;

    for (unsigned i = 0; i < numGeoms; i++)
    {
        SharedPtr<Geometry> geometry(CreateGeometry(instances, i * instancesPerGeom, instancesPerGeom));
        if (!geometry)
        {
            URHO3D_LOGINFOF("Failed to create geometry");
            continue;
        }

        geometries_[i] = geometry;

        batches_[i].geometry_ = geometry;
        batches_[i].material_ = material_;
        batches_[i].worldTransform_ = worldTransform;
    }

    OnMarkedDirty(node_);

    return true;
}

SharedPtr<Geometry> BatchedGeometry::CreateGeometry(
        const PODVector<BatchInstanceDesc>& instances,
        unsigned startInstance, unsigned count)
{
    if (instances.Size() <= 0 || startInstance >= instances.Size())
        return SharedPtr<Geometry>(0);

    if (startInstance + count > instances.Size())
        count = instances.Size() - startInstance;

    VertexBuffer* origBuffer = model_->GetGeometry(0, 0)->GetVertexBuffer(0);
    IndexBuffer* origIndex = model_->GetGeometry(0, 0)->GetIndexBuffer();

    unsigned vertexCount = origBuffer->GetVertexCount();
    unsigned vertexSize = origBuffer->GetVertexSize();

    unsigned instanceSize = vertexCount * vertexSize;

    if (vertexCount * count > USHRT_MAX)
    {
        URHO3D_LOGWARNINGF("Created geometrys vertex count (%u) would exceed USHRT_MAX (%u)",
            vertexCount * count, USHRT_MAX);

        return SharedPtr<Geometry>(NULL);
    }

    const PODVector<VertexElement> &elements = origBuffer->GetElements();

    const VertexElement *positionElem = origBuffer->GetElement(SEM_POSITION);
    const VertexElement *colorElem = origBuffer->GetElement(SEM_COLOR);

    if (!positionElem)
    {
        URHO3D_LOGERROR("Position vertex element not defined in source model geometry");
        return SharedPtr<Geometry>(NULL);
    }

    if (colorElem && !(colorElem->type_ == TYPE_UBYTE4 || colorElem->type_ == TYPE_UBYTE4_NORM))
    {
        URHO3D_LOGERRORF("Invalid vertex color type %u for BatchedGeometry", colorElem->type_);
        colorElem = NULL;
    }

    SharedPtr<Geometry> geom(new Geometry(context_));

    SharedPtr<VertexBuffer> vb(new VertexBuffer(context_));
    SharedPtr<IndexBuffer> ib(new IndexBuffer(context_));

    // Copy vertex buffer instances
    PODVector<unsigned char> tmp(count * instanceSize);
    const unsigned char* vertexData = (const unsigned char*)origBuffer->Lock(0, vertexCount);

    for (unsigned i = 0; i < count; ++i)
    {
        const BatchInstanceDesc& source = instances[startInstance + i];
        unsigned char *inst = &tmp[i * instanceSize];

        Matrix3x4 transform(source.position_, source.rotation_, source.scale_);

        // This will also copy other attributes like texture coordinates and normals
        memcpy(inst, &vertexData[0], instanceSize);

        // TODO: rotate also normals

        // Transform the vertices to their final position
        for (unsigned k = 0; k < vertexCount; ++k)
        {
            // Assume that position is the first
            Vector3 *v = reinterpret_cast<Vector3*>(inst + k * vertexSize);
            Vector3 point = *v;
            *v = transform * point;

            boundingBox_.Merge(*v);

            if (colorElem)
            {
                unsigned char *c = inst + k * vertexSize + colorElem->offset_;

                c[0] = source.color_.r_ * (float)c[0];
                c[1] = source.color_.g_ * (float)c[1];
                c[2] = source.color_.b_ * (float)c[2];
                c[3] = 0;
            }
        }
    }

    vb->SetShadowed(true);
    assert(vb->SetSize(count * vertexCount, elements));
    assert(vb->SetData(&tmp[0]));

    // Copy index buffer instances
    unsigned indexSize = origIndex->GetIndexSize();
    unsigned indexCount = origIndex->GetIndexCount();

    tmp.Resize(indexCount * indexSize * count);
    const unsigned char* indexData = (const unsigned char*)origIndex->Lock(0, indexCount);

    assert(indexData != NULL);
    assert(indexSize == sizeof(unsigned short));

    for (unsigned i = 0; i < count; ++i)
    {
        unsigned short *instance = reinterpret_cast<unsigned short*>(
            &tmp[i * indexSize * indexCount]
        );

        unsigned short offset = i * vertexCount;

        for (unsigned k = 0; k < indexCount; ++k)
        {
            instance[k] = offset + reinterpret_cast<const unsigned short *>(indexData)[k];

            assert(instance[k] < count * vertexCount);
        }
    }

    origBuffer->Unlock();
    origIndex->Unlock();

    ib->SetShadowed(true);
    assert(ib->SetSize(count * indexCount, false));
    assert(ib->SetData(&tmp[0]));

    geom->SetVertexBuffer(0, vb);
    geom->SetIndexBuffer(ib);

    assert(geom->SetDrawRange(TRIANGLE_LIST, 0, indexCount * count));

    return geom;
}

void BatchedGeometry::OnWorldBoundingBoxUpdate()
{
    worldBoundingBox_ = boundingBox_.Transformed(node_->GetWorldTransform());
}

}

}
