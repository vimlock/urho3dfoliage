#include "Patch.h"

#include "PatchLayer.h"
#include "PatchManager.h"

#include <Urho3D/Core/Context.h>
#include <Urho3D/Scene/Node.h>
#include <Urho3D/IO/Log.h>
#include <Urho3D/Graphics/DebugRenderer.h>

namespace Urho3D
{

namespace Foliage
{

Patch::Patch(Context *context):
    Component(context),

    // Some large value, will be overriden when the patch is updated
    distance_(10000.0f),
    removeTimer_(0.0f),
    keepLoaded_(false),
    dirty_(false)
{

}

void Patch::RegisterObject(Context *context)
{
    context->RegisterFactory<Patch>();

    URHO3D_ATTRIBUTE("Keep Loaded", bool, keepLoaded_, false, AM_DEFAULT);
}

void Patch::DrawDebugGeometry(DebugRenderer *debug, bool depthTest)
{
    BoundingBox box(
        Vector3(bounds_.min_.x_, 0.0f,  bounds_.min_.y_),
        Vector3(bounds_.max_.x_, 10.0f, bounds_.max_.y_)
    );

    Color color;

    if (dirty_)
        color = Color::YELLOW;
    if (keepLoaded_)
        color = Color::RED;
    else if (removeTimer_ > 0.0f)
        color = Color::GREEN;
    else
        color = Color::BLUE;

    debug->AddBoundingBox(box, color, depthTest);
}

void Patch::SetOwner(PatchManager *value)
{
    owner_ = value;
}

void Patch::SetLodDistance(float value)
{
    distance_ = value;

    for (unsigned i = 0; i < layers_.Size(); ++i)
    {
        layers_[i]->UpdateLodDistance(value, this);
    }
}

void Patch::AddLayer(PatchLayer* layer)
{
    layers_.Push(SharedPtr<PatchLayer>(layer));
    layer->UpdateLodDistance(distance_, this);
}

unsigned Patch::GetNumLayers() const
{
    return layers_.Size();
}

PatchLayer* Patch::GetLayer(PatchLoader* loader)
{
    for (unsigned i = 0; i < layers_.Size(); ++i)
    {
        if (layers_[i]->GetLoader() == loader)
            return layers_[i];
    }

    return 0;
}

PatchLayer* Patch::GetLayer(unsigned index)
{
    if (index >= layers_.Size())
        return 0;

    return layers_[index];
}

void Patch::RemoveLayer(PatchLayer* layer)
{
    if (!layer)
        return;

    layers_.Remove(SharedPtr<PatchLayer>(layer));
}

void Patch::RemoveLayer(PatchLoader *loader)
{
    if (loader)
        return;

    RemoveLayer(GetLayer(loader));
}

void Patch::RemoveLayer(unsigned index)
{
    layers_.EraseSwap(index);
}

void Patch::ClearLayers()
{
    layers_.Clear();
}

void Patch::Update(float timeStep)
{
    removeTimer_ += timeStep;

    if (removeTimer_ > 3.0f)
    {
        // If the owner still exists, let it do the unloading
        // so it can call Unload() on the patch loaders and possibly pool the scene node we use
        if (owner_)
        {
            owner_->RemovePatch(this);
        }
        else
        {
            GetNode()->Remove();
        }
    }
}

}

}
