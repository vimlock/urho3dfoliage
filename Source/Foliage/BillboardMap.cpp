#include "BillboardMap.h"

#include <Urho3D/Core/Context.h>
#include <Urho3D/Core/CoreEvents.h>

#include <Urho3D/Graphics/Camera.h>
#include <Urho3D/Graphics/Graphics.h>
#include <Urho3D/Graphics/Light.h>
#include <Urho3D/Graphics/Material.h>
#include <Urho3D/Graphics/Model.h>
#include <Urho3D/Graphics/Octree.h>
#include <Urho3D/Graphics/StaticModel.h>
#include <Urho3D/Graphics/Texture2D.h>
#include <Urho3D/Graphics/Zone.h>
#include <Urho3D/Graphics/Zone.h>

#include <Urho3D/Math/BoundingBox.h>

#include <Urho3D/Resource/ResourceCache.h>
#include <Urho3D/Resource/XMLFile.h>

#include <Urho3D/IO/FileSystem.h>
#include <Urho3D/IO/Log.h>

#include <Urho3D/Scene/Scene.h>

namespace Urho3D
{

namespace Foliage
{

static float AngleDelta(float a1, float a2)
{
    return 180.0f - Abs(Abs(a1 - a2) - 180.0f);
}

const Rect BillboardMap::GetView(const Vector3& eyePosition, const Vector3& billboardPosition, float rotationOffset)
{
    Vector3 dir = eyePosition - billboardPosition;
    dir.Normalize();

    float yaw = Asin(-dir.y_);
    float pitch = Atan2(dir.z_, dir.x_) + rotationOffset;

    return GetView(yaw, pitch);
}

const Rect BillboardMap::GetView(float yaw, float pitch) const
{
    if (pitchViews_ <= 0 || yawViews_ <= 0)
        return Rect::ZERO;

    float best = 0.0f;

    int pitchIndex = 0;
    for (int i = 0; i < pitchViews_; ++i)
    {
        float p = (90.0f / pitchViews_) * i;

        float d = AngleDelta(p, pitch);
        if (i == 0 || d < best)
        {
            best = d;
            pitchIndex = i;
        }
    }

    int yawIndex = 0;
    for (int i = 0; i < yawViews_; ++i)
    {
        float y = (360.0f / yawViews_) * i;

        float d = AngleDelta(y, yaw);
        if (i == 0 || d < best)
        {
            best = d;
            yawIndex = i;
        }
    }

    float xWidth = 1.0f / pitchViews_;
    float yWidth = 1.0f / pitchViews_;

    float xOffset = xWidth * pitchIndex;
    float yOffset = yWidth * yawIndex;

    return Rect(xOffset, yOffset, xOffset + xWidth, yOffset + yWidth);
}

Material* BillboardMap::GetMaterial() const
{
    return material_;
}

BillboardMapLoader::BillboardMapLoader(Context *context):
    Object(context),
    renderPending_(false),
    useCache_(true),
    cacheDir_("Data/Textures"),
    imageResolution_(512)
{
    SubscribeToEvent(E_UPDATE, URHO3D_HANDLER(BillboardMapLoader, OnUpdate));
}

void BillboardMapLoader::RegisterSubsystem(Context *context)
{
    context->RegisterSubsystem(new BillboardMapLoader(context));
}

BillboardMap* BillboardMapLoader::Create(XMLFile *prefab, int yawViews, int pitchViews)
{
    return Create(prefab->GetName(), prefab->GetRoot(), yawViews, pitchViews);
}

BillboardMap* BillboardMapLoader::Create(const String& name, const XMLElement &prefab,
    int yawViews, int pitchViews)
{
    BillboardMap *cached = GetCached(name, yawViews, pitchViews);
    if (cached)
        return cached;

    renderPending_ = true;

    ResourceCache *cache = GetSubsystem<ResourceCache>();

    // Unfortunaly all rendering happens at the end of the frame
    // so we have to wait at least until end of the frame to get out texture back from the GPU.

    scene_ = new Scene(context_);
    scene_->CreateComponent<Octree>();

    Node *lightNode = scene_->CreateChild("Light");
    lightNode->SetDirection(Vector3(0.5, -0.5f, 0.5f));

    Light *light = lightNode->CreateComponent<Light>();
    light->SetLightType(LIGHT_DIRECTIONAL);
    light->SetSpecularIntensity(1.0f);
    light->SetBrightness(1.0f);

    /*
    Node *boxNode = scene_->CreateChild("Box");
    boxNode->Scale(2.0f);
    {
        StaticModel *sm = boxNode->CreateComponent<StaticModel>();
        sm->SetModel(cache->GetResource<Model>("Models/Box.mdl"));
    }

    Node *boxNode2 = scene_->CreateChild("Box");
    boxNode2->Scale(2.0f);
    {
        StaticModel *sm = boxNode2->CreateComponent<StaticModel>();
        sm->SetModel(cache->GetResource<Model>("Models/Box.mdl"));
    }

    Node *boxNode3 = scene_->CreateChild("Box");
    boxNode3->Scale(2.0f);
    {
        StaticModel *sm = boxNode3->CreateComponent<StaticModel>();
        sm->SetModel(cache->GetResource<Model>("Models/Box.mdl"));
    }
    */

    Node *objectNode = scene_->CreateChild("Object");
    objectNode->LoadXML(prefab);

    BoundingBox bbox;
    if (!GetBoundingBox(objectNode, &bbox))
    {
        URHO3D_LOGERRORF("Failed to calculate bounding box for %s", name.CString());
        return 0;
    }

    Vector3 size = bbox.Size() * objectNode->GetScale();
    Vector3 center = size * 0.5f;
    float radius = size.y_ / 2.0f;
    float yOffset = (-bbox.min_.y_) * objectNode->GetScale().y_;

    /*
    boxNode2->Translate(Vector3(0.0f, radius, 0.0f));
    boxNode3->Translate(Vector3(0.0f, -radius, 0.0f));
    */

    objectNode->Translate(Vector3(0.0f, -radius + yOffset, 0.0f));

    URHO3D_LOGINFOF("Size %f, %f, %f", size.x_, size.y_, size.z_);
    URHO3D_LOGINFOF("Radius %f", radius);
    URHO3D_LOGINFOF("Translated by %f, %f, %f", center.x_, center.y_, center.z_);

    renderTexture_ = new Texture2D(context_);
    renderTexture_->SetSize(
        imageResolution_ * yawViews,
        imageResolution_ * pitchViews,
        Graphics::GetRGBAFormat(), TEXTURE_RENDERTARGET);

    renderTexture_->SetFilterMode(FILTER_BILINEAR);

    RenderSurface *renderer = renderTexture_->GetRenderSurface();
    renderer->SetNumViewports(yawViews * pitchViews);

    renderMap_ = new BillboardMap();
    renderMap_->name_ = name;
    renderMap_->views_.Reserve(yawViews * pitchViews);

    renderMap_->pitchViews_ = pitchViews;
    renderMap_->yawViews_ = yawViews;

    renderMap_->width_ = size.x_ * 0.5f;
    renderMap_->height_ = size.y_ * 0.5f;
    renderMap_->yOffset_ = yOffset;

    if (material_)
        renderMap_->material_ = material_->Clone();
    else
        renderMap_->material_ = cache->GetResource<Material>("Materials/TreeBillboard.xml");

    SharedPtr<XMLFile> renderPath(cache->GetResource<XMLFile>("RenderPaths/ForwardTransparent.xml"));

    for (int k = 0; k < pitchViews; ++k)
    {
        for (int i = 0; i < yawViews; ++i)
        {
            float pitch = (90.0f / pitchViews) * k;
            float yaw = (360.0f / yawViews) * i;

            float fov = 30.0f;
            float distance = radius / Tan(fov / 2.0f);

            URHO3D_LOGINFOF("Set camera %dx%d to %f", i, k, distance);

            Node *camNode = scene_->CreateChild("Camera");

            camNode->Yaw(yaw);
            camNode->Pitch(pitch);
            camNode->Translate(Vector3(0.0f, 0.0f, -(distance + 1.0f)));

            renderMap_->views_.Push(Rect());

            Camera *cam = camNode->CreateComponent<Camera>();
            cam->SetNearClip(distance - radius - 1.0f);
            cam->SetFarClip(distance + radius + 1.0f);
            cam->SetFov(fov);

            SharedPtr<Viewport> viewport(new Viewport(context_, scene_, cam));
            viewport->SetRect(IntRect(
                imageResolution_ * i, imageResolution_ * k,
                imageResolution_ * (i + 1), imageResolution_ * (k + 1)));
            viewport->SetRenderPath(renderPath);
            renderer->SetViewport(i * pitchViews + k, viewport);
            renderer->QueueUpdate();
        }
    }

    URHO3D_LOGINFO("Queuing render");

    return renderMap_;
}

bool BillboardMapLoader::GetBoundingBox(Node *node, BoundingBox *out)
{
    PODVector<Drawable*> drawables;
    BoundingBox bbox;

    node->GetDerivedComponents<Drawable>(drawables, true);
    if (drawables.Size() == 0)
        return false;

    for (unsigned i = 0; i < drawables.Size(); ++i)
    {
        if (i == 0)
            bbox = drawables[i]->GetBoundingBox();
        else
            bbox.Merge(drawables[i]->GetBoundingBox());
            
    }

    *out = bbox;

    return true;
}

BillboardMap* BillboardMapLoader::GetCached(const String& name, int yawViews, int pitchViews) const
{
    if (!useCache_)
        return NULL;

    String id = GetId(name, yawViews, pitchViews);

    String imgPath = cacheDir_ + "/" + id + ".png";
    String bbmPath = cacheDir_ + "/" + id + ".billboard.xml";

    ResourceCache* cache = GetSubsystem<ResourceCache>();

    /*
    SharedPtr<Texture2D> tex(cache->GetResource<Texture2D>(imgPath));
    if (tex == NULL)
        return NULL;

    SharedPtr<XMLFile> bbm(cache->GetTempResource<XMLFile>(bbmPath));
    if (bbm == NULL)
        return NULL;
    */

    return NULL;
}

String BillboardMapLoader::GetId(const String &name, int yawViews, int pitchViews)
{
    // Use yaw and pitch values in the hash to support caching different number of yaw and pitch views.
    return StringHash(ToString("%s/%d_%d", name.CString(), yawViews, pitchViews)).ToString();
}

void BillboardMapLoader::OnUpdate(StringHash eventType, VariantMap &eventData)
{
    if (!renderPending_)
        return;

    RenderSurface *renderer = renderTexture_->GetRenderSurface();
    if (renderer->IsUpdateQueued())
    {
        URHO3D_LOGWARNING("Update still queued");
        return;
    }

    renderPending_ = false;
    scene_ = NULL;

    if (GetSubsystem<Graphics>() == NULL)
    {
        URHO3D_LOGWARNING("Graphics not initialized");
        return;
    }

    if (renderMap_ == NULL)
    {
        URHO3D_LOGWARNING("Render pending but renderMap_ missing");
        return;
    }

    renderTexture_->GetRenderSurface()->SetNumViewports(0);

    SharedPtr<Image> im = renderTexture_->GetImage();
    if (im == NULL)
    {
        URHO3D_LOGWARNING("Image missing");
        return;
    }

    renderMap_->image_ = im;
    if (renderMap_->material_)
        renderMap_->material_->SetTexture(TU_DIFFUSE, renderTexture_);

    String name = StringHash(renderMap_->name_).ToString();
    String imgPath = cacheDir_ + "/" + name + ".png";
    String bbmPath = cacheDir_ + "/" + name + ".billboard.xml";

    URHO3D_LOGINFOF("Saving generated %dx%d image to %s",
        im->GetWidth(), im->GetHeight(), imgPath.CString());

    URHO3D_LOGINFOF("Saving generated billboard to %s", bbmPath.CString());

    im->SavePNG(imgPath);

    renderMap_ = NULL;
}

} // Foliage

} // Urho3D
