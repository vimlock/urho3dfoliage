#pragma once

#include "Urho3D/Scene/Component.h"

namespace Urho3D
{

namespace Foliage
{

class PatchManager;
class PatchLayer;
class PatchLoader;

class URHO3D_API Patch: public Component
{
URHO3D_OBJECT(Patch, Component);

public:
    Patch(Context *context);

    static void RegisterObject(Context *context);

    /// Visualize the component as debug geometry.
    /// Patches drawn in green are loaded and visible.
    /// Patches drawn in yellow are dirty and pending load.
    /// Patches drawn in blue are cached.
    /// Patches drawn in red are marked to stay loaded.
    virtual void DrawDebugGeometry(DebugRenderer *debug, bool depthTest);

    void MarkDirty() { dirty_ = true; }
    void MarkActive() { removeTimer_ = 0.0f; }
    void ClearDirty() { dirty_ = false; }
    void Update(float timeStep);

    void SetOwner(PatchManager *value);
    void SetKeepLoaded(bool value) { keepLoaded_ = value; };
    void SetBounds(const Rect& value) { bounds_ = value; }
    void SetLodDistance(float distance);
    void SetIndex(const IntVector2& value) { index_ = value; }
    void SetSeed(int value) { seed_ = value; }

    PatchManager* GetOwner() const { return owner_; }
    const Rect& GetBounds() const { return bounds_; }
    const IntVector2& GetIndex() const { return index_; }
    bool IsDirty() const { return dirty_; }

    int GetSeed() const { return seed_; }
    /// Add a new patch layer, no checks are made against duplicates.
    void AddLayer(PatchLayer *layer);
    /// Return number of layers.
    unsigned GetNumLayers() const;
    /// Return layer for the given loader. If the layer does not exist, NULL is returned.
    PatchLayer* GetLayer(PatchLoader *loader);
    /// Return layer by index.
    PatchLayer* GetLayer(unsigned index);
    /// Remove specific layer from the layers.
    void RemoveLayer(PatchLayer *layer);
    /// Remove layer by loader.
    void RemoveLayer(PatchLoader *loader);
    /// Remove layer by index.
    void RemoveLayer(unsigned index);
    /// Remove all layers
    void ClearLayers();

private:
    /// PatchManager which creates and manages this patch.
    WeakPtr<PatchManager> owner_;

    /// Foliage layers in this patch.
    Vector<SharedPtr<PatchLayer> > layers_;

    /// Index in the PatchManagers grid
    IntVector2 index_;

    /// Area in which the geometry added to this patch should reside.
    /// If the geometry goes outside these bounds, it will cause it to be loaded at incorrect time
    /// and might cause objects to pop in and out of the vision as the patches are loaded and unloaded.
    Rect bounds_;

    /// Random seed value used for this patch. This value is added together with
    /// the global random seed to get the final random seed per-patch.
    int seed_;

    /// How long this patch has been outside vision range
    float removeTimer_;

    /// Last calculated distance from the camera.
    float distance_;

    /// Should the patch remain loaded. Does not prevent patch from being reloaded.
    bool keepLoaded_;

    /// Should the page be reloaded
    bool dirty_;
};

} // Foliage

} // Urho3D
