#pragma once

#include <Urho3D/AngelScript/APITemplates.h>

namespace Urho3D
{

namespace Foliage
{

class PatchLoader;

/// Register the Foliage library to script.
void RegisterFoliageAPI(asIScriptEngine* engine);

/// Template function for registering a class derived from PatchLoader.
template <class T> void RegisterPatchLoader(asIScriptEngine* engine, const char* className, bool nodeRegistered = true, bool debugRendererRegistered = true)
{
}

}

}
