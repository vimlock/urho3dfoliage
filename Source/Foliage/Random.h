#pragma once

namespace Urho3D
{

namespace Foliage
{

// Custom random generator which we will
// use to get deterministic random numbers
class Random
{
public:
    Random(int seed);

    int GetSeed() const;
    void SetSeed(int seed);

    int NextInt();
    float NextFloat();
    float NextFloat(float range);
    float NextFloat(float min, float max);

private:
    unsigned seed_;
};

}

}
