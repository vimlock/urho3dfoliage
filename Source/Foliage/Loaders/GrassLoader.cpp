#include "GrassLoader.h"

#include "Foliage/BatchedGeometry.h"
#include "Foliage/Foliage.h"
#include "Foliage/Patch.h"
#include "Foliage/PatchManager.h"
#include "Foliage/Random.h"

#include <Urho3D/Core/Context.h>

#include <Urho3D/Graphics/BillboardSet.h>
#include <Urho3D/Graphics/Material.h>
#include <Urho3D/Graphics/Model.h>
#include <Urho3D/Graphics/Terrain.h>
#include <Urho3D/Resource/ResourceCache.h>
#include <Urho3D/Resource/ResourceEvents.h>
#include <Urho3D/Resource/Image.h>

#include <Urho3D/IO/Log.h>

namespace Urho3D
{

namespace Foliage
{

const char *grassModeNames[] = {
    "Billboard",
    "Model",
    0
};

const char *distributionNames[] = {
    "Even",
    "Jittered",
    "Random",
    0
};

const char *channelSourceNames[] = {
    "Red",
    "Green",
    "Blue",
    "Alpha",
    "RGB",
    0
};

const char *samplingTypeNames[] = {
    "Nearest",
    "Bilinear",
    0
};

#define DEFAULT_DISTRIBUTION DISTRIBUTION_RANDOM
#define DEFAULT_SAMPLING SAMPLING_BILINEAR
#define DEFAULT_CHANNEL CHANNEL_RED

GrassLayer::GrassLayer(GrassLoader *loader):
    loader_(loader)
{
}

PatchLoader* GrassLayer::GetLoader() const
{
    return loader_;
}

void GrassLayer::UpdateLodDistance(float newDistance, Patch *patch)
{
    // Do nothing
}

GrassLoader::GrassLoader(Context *context):
    PatchLoader(context),
    mode_(MODE_MODEL),

    billboardSize_(Vector2::ONE),
    billboardUV_(Rect::FULL),

    yOffset_(0.0f),
    randomTilt_(0.0f),
    normalInfluence_(1.0f),
    scaleMin_(1.0f, 1.0f),
    scaleMax_(1.0f, 1.0f),
    density_(1.0f),
    distribution_(DISTRIBUTION_RANDOM),
    tintMin_(Color::WHITE),
    tintMax_(Color::WHITE),
    tintNoise_(1.0f),
    seed_(0),
    densityMap_(),
    densityChannel_(CHANNEL_RED),
    densitySampling_(SAMPLING_BILINEAR)
{
}

GrassLoader::~GrassLoader()
{
}

void GrassLoader::RegisterObject(Context *context)
{
    context->RegisterFactory<GrassLoader>(FOLIAGE_CATEGORY);

    URHO3D_ENUM_ATTRIBUTE("Mode", mode_, grassModeNames, MODE_MODEL, AM_DEFAULT);

    URHO3D_MIXED_ACCESSOR_ATTRIBUTE("Material", GetMaterialAttr, SetMaterialAttr,
        ResourceRef, ResourceRef(Material::GetTypeStatic()), AM_DEFAULT);

    URHO3D_MIXED_ACCESSOR_ATTRIBUTE("Model", GetModelAttr, SetModelAttr,
        ResourceRef, ResourceRef(Model::GetTypeStatic()), AM_DEFAULT);

    URHO3D_ATTRIBUTE("Billboard Size", Vector2, billboardSize_, Vector2::ONE, AM_DEFAULT);
    URHO3D_ACCESSOR_ATTRIBUTE("Billboard UV", GetUV, SetUV, Rect, Rect::FULL, AM_DEFAULT);

    URHO3D_ATTRIBUTE("Y Offset" , float, yOffset_, 0.0f, AM_DEFAULT);
    URHO3D_ACCESSOR_ATTRIBUTE("Random Tilt", GetRandomTilt, SetRandomTilt, float, 0.0f, AM_DEFAULT);
    URHO3D_ACCESSOR_ATTRIBUTE("Normal Influence", GetNormalInfluence, SetNormalInfluence, float, 1.0f, AM_DEFAULT);

    URHO3D_ATTRIBUTE("Scale Min", Vector2, scaleMin_, Vector2(1.0f, 1.0f), AM_DEFAULT);
    URHO3D_ATTRIBUTE("Scale Max", Vector2, scaleMax_, Vector2(1.0f, 1.0f), AM_DEFAULT);
    URHO3D_ATTRIBUTE("Density", float, density_, 1.0f, AM_DEFAULT);

    URHO3D_ENUM_ATTRIBUTE("Distribution", distribution_, distributionNames, DEFAULT_DISTRIBUTION, AM_DEFAULT);

    URHO3D_ATTRIBUTE("Tint Min", Color, tintMin_, Color::WHITE, AM_DEFAULT);
    URHO3D_ATTRIBUTE("Tint Max", Color, tintMax_, Color::WHITE, AM_DEFAULT);
    URHO3D_ATTRIBUTE("Tint Noise", float, tintNoise_, 1.0f, AM_DEFAULT);

    URHO3D_ATTRIBUTE("Seed", int, seed_, 0, AM_DEFAULT);

    URHO3D_MIXED_ACCESSOR_ATTRIBUTE("Density Map", GetDensityMapAttr, SetDensityMapAttr, ResourceRef, ResourceRef(Image::GetTypeStatic()), AM_DEFAULT);
    URHO3D_ENUM_ATTRIBUTE("Density Channel", densityChannel_, channelSourceNames, DEFAULT_CHANNEL, AM_DEFAULT);
    URHO3D_ENUM_ATTRIBUTE("Density Sampling", densitySampling_, samplingTypeNames, DEFAULT_SAMPLING, AM_DEFAULT);

    URHO3D_COPY_BASE_ATTRIBUTES(PatchLoader);
}

void GrassLoader::Update(PatchManager *pm, float timeStep)
{
    UpdateMaterial(material_);
}

void GrassLoader::OnDensityMapReloadFinished(StringHash eventType, VariantMap& eventData)
{
    MarkDirty();
}

void GrassLoader::Load(Patch *patch)
{
    PODVector<Vector2> positions;

    GetPositions(patch, positions);

    // URHO3D_LOGINFOF("Planting %u grass instances", positions.Size());

    if (mode_ == MODE_MODEL)
        PlantBatchedGeometry(patch, positions);
    else
        PlantBillboard(patch, positions);
}

void GrassLoader::Unload(Patch *patch)
{
}

void GrassLoader::SetModel(Model *value)
{
    model_ = value;
}

static void CheckGrassShaderParameter(Material *material, const char *param)
{
    if (material->GetShaderParameter(param) == Variant::EMPTY)
        URHO3D_LOGWARNINGF("Material %s does not have %s shader parameter",
            material->GetName().CString(), param);
}

void GrassLoader::UpdateMaterial(Material* material)
{
    PatchManager *pm = GetPatchManager();
    if (!pm)
        return;

    float farEnd = pm->GetLoadDistance();
    float farStart = farEnd - pm->GetPatchSize().Length();

    Vector4 params(0.0f, 0.0f, farStart, farEnd);
    material->SetShaderParameter("FoliageFadeParams", params);
    material->SetVertexShaderDefines("FOLIAGE_FAR_FADE");
    material->SetPixelShaderDefines("FOLIAGE_FAR_FADE");
}

void GrassLoader::SetMaterial(Material *value)
{
    if (!value)
    {
        material_ = 0;
        return;
    }

    material_ = value;

    CheckGrassShaderParameter(material_, "WindPeriod");
    CheckGrassShaderParameter(material_, "WindHeightFactor");
    CheckGrassShaderParameter(material_, "WindHeightPivot");
    CheckGrassShaderParameter(material_, "WindWorldSpacing");

    CheckGrassShaderParameter(material_, "FoliageFadeStart");
    CheckGrassShaderParameter(material_, "FoliageFadeEnd");
}

void GrassLoader::SetDensityMap(Image *image)
{
    if (image && image->IsCompressed())
    {
        URHO3D_LOGWARNING("Can not use compressed image as density map");
        return;
    }

    if (densityMap_)
        UnsubscribeFromEvent(densityMap_, E_RELOADFINISHED);

    if (image)
        SubscribeToEvent(image, E_RELOADFINISHED, URHO3D_HANDLER(GrassLoader, OnDensityMapReloadFinished));

    densityMap_ = image;
}

void GrassLoader::SetRandomTilt(float value)
{
    randomTilt_ = Clamp(value, 0.0f, 90.0f);
}

void GrassLoader::SetNormalInfluence(float value)
{
    normalInfluence_ = Clamp(value, 0.0f, 2.0f);
}

void GrassLoader::SetMaterialAttr(const ResourceRef &value)
{
    ResourceCache *cache = GetSubsystem<ResourceCache>();
    SetMaterial(cache->GetResource<Material>(value.name_));
}

void GrassLoader::SetModelAttr(const ResourceRef &value)
{
    ResourceCache *cache = GetSubsystem<ResourceCache>();
    SetModel(cache->GetResource<Model>(value.name_));
}

void GrassLoader::SetDensityMapAttr(const ResourceRef &value)
{
    ResourceCache *cache = GetSubsystem<ResourceCache>();
    SetDensityMap(cache->GetResource<Image>(value.name_));
}


ResourceRef GrassLoader::GetDensityMapAttr() const
{
    return GetResourceRef(densityMap_, Image::GetTypeStatic());
}


ResourceRef GrassLoader::GetModelAttr() const
{
    return GetResourceRef(model_, Model::GetTypeStatic());
}

ResourceRef GrassLoader::GetMaterialAttr() const
{
    return GetResourceRef(material_, Material::GetTypeStatic());
}

Model* GrassLoader::GetModel() const
{
    return model_;
}

Material* GrassLoader::GetMaterial() const
{
    return material_;
}

void GrassLoader::OnSetAttribute(const AttributeInfo& attr, const Variant& src)
{
    Serializable::OnSetAttribute(attr, src);

    // Changing any attribute will force us to regenerate this foliage type
    MarkDirty();
}

float GrassLoader::GetMapValue(Image *image, float x, float y, ChannelSource channel, SamplingType sampling)
{
    Color col = GetMapColor(image, x, 1.0f - y, sampling);

    switch (channel)
    {
    case CHANNEL_RED:   return col.r_;
    case CHANNEL_GREEN: return col.g_;
    case CHANNEL_BLUE:  return col.b_;
    case CHANNEL_ALPHA: return col.a_;
    case CHANNEL_RGB:   return col.Luma();
    }

    // Hush compiler
    return 0.0f;
}

Color GrassLoader::GetMapColor(Image *image, float x, float y, SamplingType sampling)
{
    if (!image)
    {
        URHO3D_LOGWARNING("GetMapColor() without image");
        return Color::BLACK;
    }

    if (sampling == SAMPLING_NEAREST)
        return image->GetPixel(image->GetWidth() * x, image->GetHeight() * y);
    else
        return image->GetPixelBilinear(x, y);
}

void GrassLoader::GetPositions(Patch *patch, PODVector<Vector2> &positions)
{
    if (density_ <= 0.0f)
        return;

    PatchManager *pm = patch->GetOwner();
    Vector2 terrainSize = pm->GetSize();
    Vector2 halfSize = terrainSize * 0.5f;

    Rect bounds = patch->GetBounds();
    Vector2 size = bounds.Size();
    float area = size.x_ * size.y_;

    Random rand(seed_ ^ patch->GetSeed());

    if (distribution_ == DISTRIBUTION_RANDOM)
    {
        unsigned numGrass = (unsigned)(area / density_);

        if (numGrass > 10000)
        {
            URHO3D_LOGWARNING("Grass limit reached (10000)");
            numGrass = 10000;
        }

        // Let's take a guess, 
        positions.Reserve(numGrass * 0.5f);

        for (unsigned i = 0; i < numGrass; ++i)
        {
            Vector2 position(
                rand.NextFloat(bounds.min_.x_, bounds.max_.x_),
                rand.NextFloat(bounds.min_.y_, bounds.max_.y_)
            );

            float chance = 1.0f;

            if (densityMap_.NotNull())
            {
                Vector2 texcoord = (halfSize + position) / terrainSize;

                chance *= GetMapValue(densityMap_, texcoord.x_, texcoord.y_,
                    densityChannel_, densitySampling_);
            }

            if (rand.NextFloat(0.0f, 1.0f) > chance)
                continue;

            positions.Push(position);
        }
    }
    else 
    {
        int xRows = size.x_ / density_;
        int yRows = size.y_ / density_;

        float xSpacing = size.x_ / xRows;
        float ySpacing = size.y_ / yRows;

        float xJitter;
        float yJitter;

        // Let's take a guess, 
        positions.Reserve(xRows * yRows * 0.5f);

        if (distribution_ == DISTRIBUTION_JITTERED)
        {
            xJitter = xSpacing;
            yJitter = ySpacing;
        }
        else
        {
            xJitter = 0.0f;
            yJitter = 0.0f;
        }

        for (int x = 0; x < xRows; ++x)
        {
            for (int y = 0; y < yRows; ++y)
            {
                Vector2 position(
                    position.x_ = bounds.min_.x_ + (x * xSpacing) + rand.NextFloat(-xJitter, xJitter),
                    position.y_ = bounds.min_.y_ + (y * ySpacing) + rand.NextFloat(-yJitter, yJitter)
                );

                float chance = 1.0f;

                if (densityMap_.NotNull())
                {
                    Vector2 texcoord = (halfSize + position) / terrainSize;

                    chance *= GetMapValue(densityMap_, texcoord.x_, texcoord.y_,
                        densityChannel_, densitySampling_);
                }

                if (rand.NextFloat(0.0f, 1.0f) > chance)
                    continue;

                positions.Push(position);
            }
        }
    }
}

void GrassLoader::PlantBillboard(Patch *patch, const PODVector<Vector2> &positions)
{
    if (material_ == NULL)
    {
        URHO3D_LOGWARNING("Material not assigned to GrassLoader");
        return;
    }

    Terrain *terrain = patch->GetOwner()->GetTerrain();
    Vector3 origin = patch->GetNode()->GetPosition();

    BillboardSet *bs = patch->GetNode()->CreateComponent<BillboardSet>();
    bs->SetMaterial(material_);
    bs->SetNumBillboards(positions.Size());
    bs->SetFaceCameraMode(FC_ROTATE_Y);

    AssignDrawableAttributes(bs);

    Random rand(seed_ ^ patch->GetSeed());

    for (unsigned i = 0; i < positions.Size(); ++i)
    {
        GrassParams params;
        GetGrassParams(&params, positions[i], rand, terrain);

        Billboard* b = bs->GetBillboard(i);
        b->position_ = params.position_ - origin;
        b->size_ = billboardSize_ * params.scale_;
        b->color_ = params.color_;
        b->uv_ = billboardUV_;
        b->enabled_ = true;
        b->rotation_ = 0.0f;
    }

    bs->Commit();
}

void GrassLoader::PlantBatchedGeometry(Patch *patch, const PODVector<Vector2> &positions)
{
    if (material_ == NULL)
    {
        URHO3D_LOGWARNING("Material not assigned to GrassLoader");
        return;
    }

    if (model_ == NULL)
    {
        URHO3D_LOGWARNING("Model not assigned to GrassLoader");
        return;
    }

    Terrain *terrain = patch->GetOwner()->GetTerrain();
    Vector3 origin = patch->GetNode()->GetPosition();
    PODVector<BatchInstanceDesc> instances;

    Random rand(seed_ ^ patch->GetSeed());

    for (unsigned i = 0; i < positions.Size(); ++i)
    {
        BatchInstanceDesc desc;
        GrassParams params;
        GetGrassParams(&params, positions[i], rand, terrain);

        desc.position_ = params.position_ - origin;
        desc.rotation_ = params.rotation_;
        desc.scale_ = Vector3(params.scale_.x_, params.scale_.y_, params.scale_.x_);
        desc.color_ = params.color_;

        instances.Push(desc);
    }

    BatchedGeometry *mesh = patch->GetNode()->CreateComponent<BatchedGeometry>();
    mesh->Create(model_, material_, instances);

    AssignDrawableAttributes(mesh);
}

static float Noise(const Vector2& position)
{
    return Mod(Sin(position.DotProduct(Vector2(12.9898, 78.233)) * M_RADTODEG), 1.0f);
}

void GrassLoader::GetGrassParams(GrassParams *params, const Vector2 &position, Random &rand, Terrain *terrain)
{
    Vector3 point(
        position.x_,
        0.0f,
        position.y_
    );


    point.y_ = terrain->GetHeight(point);

    Quaternion normal = Quaternion(Vector3::UP, terrain->GetNormal(point)) * normalInfluence_;
    Quaternion tilt = Quaternion(rand.NextFloat(1.0f) * randomTilt_, 0.0f, 0.0f);
    Quaternion forward = Quaternion(0.0f, rand.NextFloat(360.0f), 0.0f);

    params->position_ = point;
    params->position_.y_ += yOffset_;
    params->rotation_ = normal * forward * tilt;

    params->scale_ = Vector2(
        rand.NextFloat(scaleMin_.x_, scaleMax_.x_),
        rand.NextFloat(scaleMin_.y_, scaleMax_.y_)
    );

    params->color_ = Lerp(tintMin_, tintMax_, Noise(Vector2(point.x_, point.z_) * tintNoise_));
}

} // Foliage

} // Urho3D
