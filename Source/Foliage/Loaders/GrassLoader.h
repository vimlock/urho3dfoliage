#pragma once

#include "Foliage/PatchLayer.h"
#include "Foliage/PatchLoader.h"

namespace Urho3D
{

class Model;
class Material;
class Terrain;
class Image;

namespace Foliage
{

class Random;
class GrassLoader;
class BatchedGeometry;

enum GrassMode
{
    MODE_BILLBOARD,
    MODE_MODEL
};

enum Distribution
{
    DISTRIBUTION_EVEN,
    DISTRIBUTION_JITTERED,
    DISTRIBUTION_RANDOM
};

enum ChannelSource
{
    CHANNEL_RED,
    CHANNEL_GREEN,
    CHANNEL_BLUE,
    CHANNEL_ALPHA,
    CHANNEL_RGB
};

enum SamplingType
{
    SAMPLING_NEAREST,
    SAMPLING_BILINEAR
};

extern const char *grassModeNames[];
extern const char *channelSourceNames[];
extern const char *samplingTypeNames[];

struct GrassParams
{
    Vector3 position_;
    Quaternion rotation_;
    Vector2 scale_;
    Color color_;
};

class URHO3D_API GrassLayer : public PatchLayer
{
public:
    GrassLayer(GrassLoader* loader);

    virtual PatchLoader* GetLoader() const;
    virtual void UpdateLodDistance(float newDistance, Patch *patch);

private:
    /// The generated grass in this layer.
    WeakPtr<BatchedGeometry> grass_;

    /// Loader which was used to generate this layer.
    WeakPtr<GrassLoader> loader_;
};

class URHO3D_API GrassLoader : public PatchLoader
{
URHO3D_OBJECT(GrassLoader, PatchLoader);

public:
    /// Construct.
    GrassLoader(Context *context);

    /// Destruct.
    ~GrassLoader();

    /// Register object factory.
    static void RegisterObject(Context *context);

    /// Handle attribute write access
    virtual void OnSetAttribute(const AttributeInfo& attr, const Variant& src);

    /// Called every frame
    virtual void Update(PatchManager *pm, float timeStep);
    /// Called when PatchManager wants us to generate a new patch
    virtual void Load(Patch *patch);
    /// Called when a FoliagePatch is no longer needed
    virtual void Unload(Patch *patch);

    void SetMode(GrassMode value) { mode_ = value; }
    void SetModel(Model *value);
    void SetMaterial(Material *value);

    /// Set billboard UV's.
    void SetUV(const Rect &value) { billboardUV_ = value; }

    /// Set the Y offset.
    void SetYOffset(float value) { yOffset_ = value; }
    /// Set random tilt.
    void SetRandomTilt(float value);
    /// Set normal influence.
    void SetNormalInfluence(float value);
    /// Set scale.
    void SetMinScale(const Vector2& value) { scaleMin_ = value; }
    /// Set random scale.
    void SetMaxScale(const Vector2& value) { scaleMax_ = value; }
    /// Set density.
    void SetDensity(float value) { density_ = value; }
    /// Set distribution method.
    void SetDistribution(Distribution value) { distribution_ = value; }
    void SetTintMin(const Color& value) { tintMin_ = value; }
    void SetTintMax(const Color& value) { tintMax_ = value; }
    void SetTintNoise(float value) { tintNoise_ = value; }
    /// Set the random seed.
    void SetSeed(int value) { seed_ = value; }


    /// Returns the billboard UV's.
    const Rect& GetUV() const { return billboardUV_; };
    /// Return the y offset.
    float GetYOffset() const { return yOffset_; }
    /// Return random tilt influence.
    float GetRandomTilt() const { return randomTilt_; }
    /// Return terrain normal influence.
    float GetNormalInfluence() const { return normalInfluence_; }
    /// Return minimum scale.
    const Vector2& GetMinScale() const { return scaleMin_; }
    /// Return maximum scale.
    const Vector2& GetMaxScale() const { return scaleMax_; }
    /// Return density.
    float GetDensity() const { return density_; }
    /// Return distribution method.
    Distribution GetDistribution() const { return distribution_; }
    Color GetTintMin() const { return tintMin_; }
    Color GetTintMax() const { return tintMax_; }
    float GetTintNoise() const { return tintNoise_; }
    /// Return the random seed.
    int GetSeed() const;

    void SetDensityMap(Image *image);

    GrassMode GetMode() const { return mode_; }
    Model* GetModel() const;
    Material* GetMaterial() const;
    Image* GetDensityMap() const { return densityMap_; }

    void SetModelAttr(const ResourceRef &value);
    void SetMaterialAttr(const ResourceRef &value);
    void SetDensityMapAttr(const ResourceRef &densityMap);

    ResourceRef GetModelAttr() const;
    ResourceRef GetMaterialAttr() const;
    ResourceRef GetDensityMapAttr() const;

private:
    static float GetMapValue(Image *image, float x, float y, ChannelSource channel, SamplingType sampling);
    static Color GetMapColor(Image *image, float x, float y, SamplingType sampling);

    void OnDensityMapReloadFinished(StringHash eventType, VariantMap& eventData);

    void PlantBillboard(Patch *patch, const PODVector<Vector2> &positions);
    void PlantBatchedGeometry(Patch *patch, const PODVector<Vector2> &positions);

    void GetPositions(Patch *patch, PODVector<Vector2> &positions);
    void GetGrassParams(GrassParams *params, const Vector2 &position, Random &rand, Terrain *terrain);

    void UpdateMaterial(Material* material);

    /// Rendering mode to use.
    GrassMode mode_;

    /// Model which will be the source for the GeometryBatch generated by this loader.
    /// Only used when mode == MODEL
    SharedPtr<Model> model_;

    /// Material to use for billboard and models.
    /// For performance reasons only one material type is allowed.
    SharedPtr<Material> material_;

    /// World size of the billboards
    Vector2 billboardSize_;

    /// Texture coordinates of the billboards
    Rect billboardUV_;

    /// Offset applied to each invidivual mesh piece in the rotated normals direction.
    /// This can be used to place the foliage more tightly on ground.
    float yOffset_;

    // Random rotation around origin, in degrees, applied to each mesh piece.
    float randomTilt_;

    /// How much the terrains normal influences the upward direction of an invidual mesh piece, [0.0 - 1.0]
    float normalInfluence_;

    /// Minimum scale
    Vector2 scaleMin_;

    /// Maximum scale
    Vector2 scaleMax_;

    // Number of grass instances per 1x1 area assuming all of them get planted
    float density_;

    /// Method to use when choosing potential spawning locations for
    /// grass instances.
    Distribution distribution_;

    /// Minimum tint color, only works with vertex colors.
    Color tintMin_;

    /// Maximum tint color, only works with vertex colors.
    Color tintMax_;

    /// Amount of randomness in the tint colors.
    float tintNoise_;
    
    /// Initial seed number for RNG which is used to plant this foliage type
    int seed_;

    SharedPtr<Image> densityMap_;
    ChannelSource densityChannel_;
    SamplingType densitySampling_;
};

} // Foliage

} // Urho3D
