#include "TreeLoader.h"

#include "Foliage/BillboardMap.h"
#include "Foliage/Foliage.h"
#include "Foliage/Patch.h"
#include "Foliage/PatchManager.h"

#include <Urho3D/Core/Context.h>

#include <Urho3D/Graphics/BillboardSet.h>
#include <Urho3D/Graphics/Material.h>
#include <Urho3D/Graphics/Model.h>
#include <Urho3D/Graphics/Terrain.h>

#include <Urho3D/Resource/ResourceCache.h>
#include <Urho3D/Resource/XMLFile.h>

#include <Urho3D/IO/Log.h>

namespace Urho3D
{

namespace Foliage
{

static const char *treeInfoStructureNames[] = {
    "Tree Count",
    "   Position",
    "   Rotation",
    "   Scale",
    0
};

Tree::Tree(Context *context):
    Component(context),
    index_(0)
{
}

Tree::Tree(Context *context, unsigned index, const TreeInfo &info, TreeLoader *loader):
    Component(context),
    index_(index),
    info_(info),
    loader_(loader)
{
}

Tree::~Tree()
{
}

TreeLayer::TreeLayer(TreeLoader *loader):
    loader_(loader),
    modelsLoaded_(false),
    billboardsLoaded_(false)
{
}

void TreeLayer::UpdateLodDistance(float newDistance, Patch *patch)
{
    if (!loader_)
        return;

    bool loadModels = false;
    bool loadBillboards = false;

    if (newDistance > loader_->billboardDistance_)
    {
        loadBillboards = true;
    }

    if (newDistance < loader_->billboardDistance_ + loader_->billboardFadeDistance_)
    {
        loadModels = true;
    }

    if (loadModels)
    {
        if (!modelsLoaded_)
        {
            loader_->LoadModels(patch, this);
            modelsLoaded_ = true;
        }

        for (unsigned i = 0; i < models_.Size(); ++i)
        {
            Node *node = models_[i];
            if (node)
                node->SetEnabled(true);
        }
    }
    else if (modelsLoaded_)
    {
        for (unsigned i = 0; i < models_.Size(); ++i)
        {
            Node *node = models_[i];
            if (node)
                node->SetEnabled(false);
        }
    }

    Vector3 camera = patch->GetOwner()->GetViewPosition();
    Vector3 dir = camera - patch->GetNode()->GetPosition();
    dir.Normalize();

    bool force = false;
    if (billboardsLoaded_)
    {
        if (Abs(dir.DotProduct(oldDir)) < 0.5)
            force = true;
    }

    if (loadBillboards || force)
    {
        if (force || !billboardsLoaded_)
        {
            loader_->LoadBillboards(patch, this);
            billboardsLoaded_ = true;
            oldDir = dir;
        }

        billboards_->SetEnabled(true);
    }
    else if (billboardsLoaded_)
    {
        billboards_->SetEnabled(false);
    }
}

PatchLoader* TreeLayer::GetLoader() const
{
    return loader_;
}

TreeLoader::TreeLoader(Context *context):
    PatchLoader(context),

    billboardDistance_(200.0f),
    billboardFadeDistance_(50.0f),
    createTreeComponents_(true)
{
}

TreeLoader::~TreeLoader()
{
}

void TreeLoader::RegisterObject(Context *context)
{
    context->RegisterFactory<TreeLoader>(FOLIAGE_CATEGORY);

    URHO3D_ACCESSOR_ATTRIBUTE("Billboard Distance", GetBillboardDistance, SetBillboardDistance, float, 200.0f, AM_DEFAULT);
    URHO3D_ATTRIBUTE("Billboard Fade Distance", float, billboardFadeDistance_, 5.0f, AM_DEFAULT);
    URHO3D_ATTRIBUTE("Create Components", bool, createTreeComponents_, true, AM_DEFAULT);

    URHO3D_MIXED_ACCESSOR_ATTRIBUTE("Tree Prefab",
        GetTreePrefabAttr, SetTreePrefabAttr, ResourceRef, ResourceRef(XMLFile::GetTypeStatic()), AM_DEFAULT);

    // Trees are marked as AM_NOEDIT because editor can't handle this many objects
    URHO3D_MIXED_ACCESSOR_VARIANT_VECTOR_STRUCTURE_ATTRIBUTE("Trees",
        GetTreesAttr, SetTreesAttr, VariantVector, Variant::emptyVariantVector,
        treeInfoStructureNames, AM_FILE | AM_NOEDIT
    );
    
    URHO3D_COPY_BASE_ATTRIBUTES(PatchLoader);
}

void TreeLoader::Load(Patch *patch)
{
    if (treePrefab_.Null())
    {
        URHO3D_LOGWARNINGF("No tree prefab assigned");
        return;
    }

    patch->RemoveLayer(this);
    SharedPtr<TreeLayer> layer(new TreeLayer(this));
    patch->AddLayer(layer);
}

void TreeLoader::Update(PatchManager *pm, float timeStep)
{
    if (!billboardMap_)
        return;

    UpdateMaterial(billboardMap_->GetMaterial());
}

void TreeLoader::UpdateMaterial(Material* material)
{
    if (!material)
        return;

    PatchManager *pm = GetPatchManager();
    if (!pm)
        return;

    float nearStart = billboardDistance_;
    float nearEnd = billboardDistance_ + billboardFadeDistance_;

    float farEnd = pm->GetLoadDistance();
    float farStart = farEnd - pm->GetPatchSize().Length();

    Vector4 params(nearStart, nearEnd, farStart, farEnd);
    material->SetShaderParameter("FoliageFadeParams", params);

    material->SetVertexShaderDefines("FOLIAGE_NEAR_FADE");
    material->SetPixelShaderDefines("FOLIAGE_NEAR_FADE");

    material->SetVertexShaderDefines("FOLIAGE_FAR_FADE");
    material->SetPixelShaderDefines("FOLIAGE_FAR_FADE");

}

void TreeLoader::GetTreeIndices(PODVector<unsigned>& values, const Rect& bounds) const
{
    for (unsigned i = 0; i < trees_.Size(); ++i)
    {
        const TreeInfo &tree = trees_[i];
        if (!bounds.IsInside(tree.position_))
            continue;

        values.Push(i);
    }
}

void TreeLoader::LoadBillboards(Patch *patch, TreeLayer *layer)
{
    Terrain *terrain = patch->GetOwner()->GetTerrain();
    if (terrain == NULL)
        return;

    if (!billboardMap_)
    {
        URHO3D_LOGWARNING("No billboard map generated");
        return;
    }
    
    Material *billboardMaterial = billboardMap_->GetMaterial();
    if (!billboardMaterial)
    {
        URHO3D_LOGWARNING("No billboard material generated");
        return;
    }

    Rect bounds = patch->GetBounds();
    PODVector<unsigned> treeIndices;

    GetTreeIndices(treeIndices, bounds);

    BillboardSet *bs = layer->billboards_;
    if (!bs)
    {
        bs = patch->GetNode()->CreateComponent<BillboardSet>();
        layer->billboards_ = bs;
    }

    bs->SetMaterial(billboardMaterial);
    bs->SetNumBillboards(treeIndices.Size());
    bs->SetFaceCameraMode(FC_ROTATE_XYZ);

    // bs->SetMaterial(GetSubsystem<ResourceCache>()->GetResource<Material>("Materials/DefaultGrey.xml"));

    AssignDrawableAttributes(bs);

    Vector3 origin = patch->GetNode()->GetWorldPosition();
    Vector3 camera = patch->GetOwner()->GetViewPosition();


    for (unsigned i = 0; i < treeIndices.Size(); ++i)
    {
        const TreeInfo &tree = trees_[treeIndices[i]];

        Billboard* b = bs->GetBillboard(i);

        b->size_ = Vector2(
            billboardMap_->GetWidth() * tree.scale_.x_,
            billboardMap_->GetHeight() * tree.scale_.y_
        );

        Vector3 position(tree.position_.x_, 0.0f, tree.position_.y_);
        position.y_ = terrain->GetHeight(position) +
            b->size_.y_ - billboardMap_->GetYOffset() * tree.scale_.y_;

        b->position_ = position - origin;

        b->color_ = Color::WHITE;
        b->uv_ = billboardMap_->GetView(camera, position, tree.rotation_);
        b->enabled_ = true;
        b->rotation_ = 0.0f;
    }

    bs->Commit();
}

void TreeLoader::LoadModels(Patch *patch, TreeLayer *layer)
{
    const XMLElement &prefabRoot = treePrefab_->GetRoot();

    Terrain *terrain = patch->GetOwner()->GetTerrain();
    if (terrain == NULL)
        return;

    Rect bounds = patch->GetBounds();
    PODVector<unsigned> treeIndices;

    // Initialize drawables container outside the loop so it doesn't get reallocated every loop
    PODVector<Drawable*> drawables;

    GetTreeIndices(treeIndices, bounds);

    for (unsigned i = 0; i < treeIndices.Size(); ++i)
    {
        const TreeInfo &tree = trees_[treeIndices[i]];

        Vector3 position(tree.position_.x_, 0.0f, tree.position_.y_);
        position.y_ = terrain->GetHeight(position);

        Node *node = patch->GetNode()->CreateTemporaryChild(String("Tree_" + i), LOCAL);
        node->LoadXML(prefabRoot);

        node->GetDerivedComponents<Drawable>(drawables, true);
        for (unsigned k = 0; k < drawables.Size(); ++k)
            AssignDrawableAttributes(drawables[k]);

        Vector3 offset = node->GetPosition();
        node->SetWorldPosition(position);
        node->Translate(offset);

        node->Scale(Vector3(tree.scale_.x_, tree.scale_.y_, tree.scale_.x_));
        node->Yaw(tree.rotation_);

        layer->models_.Push(WeakPtr<Node>(node));
    }
}

bool TreeLoader::AddInstance(const TreeInfo &instance)
{
    trees_.Push(instance);
    MarkDirty();

    return true;
}

bool TreeLoader::AddInstanceHelper(const Vector2& position, float rotation, const Vector2& scale)
{
    TreeInfo tree;

    tree.color_ = Color::WHITE;
    tree.position_ = position;
    tree.rotation_ = rotation;
    tree.scale_ = scale;

    return AddInstance(tree);
}

void TreeLoader::RemoveInstance(unsigned index)
{
}

void TreeLoader::RemoveInstances()
{
    trees_.Clear();
    MarkDirty();
}


void TreeLoader::SetPrefab(XMLFile *file)
{
    treePrefab_ = file;
    
    if (treePrefab_)
    {
        BillboardMapLoader *bml = GetSubsystem<BillboardMapLoader>();
        billboardMap_ = bml->Create(file, 4, 4);
    }
    else
    {
        billboardMap_ = NULL;
    }
}

void TreeLoader::SetTreePrefabAttr(const ResourceRef& value)
{
    ResourceCache *cache = GetSubsystem<ResourceCache>();
    SetPrefab(cache->GetResource<XMLFile>(value.name_));
}

ResourceRef TreeLoader::GetTreePrefabAttr() const
{
    return GetResourceRef(treePrefab_, XMLFile::GetTypeStatic());
}

void TreeLoader::SetTreesAttr(const VariantVector& value)
{
    unsigned index = 0;
    unsigned numTrees = value.Size() > 0 ? value[index++].GetUInt() : 0;

    trees_.Clear();
    trees_.Resize(numTrees);

    for (PODVector<TreeInfo>::Iterator i = trees_.Begin();
            i != trees_.End() && index < value.Size(); ++i)
    {
        i->color_ = value[index++].GetColor();
        i->position_ = value[index++].GetVector2();
        i->rotation_ = value[index++].GetFloat();
        i->scale_ = value[index++].GetVector2();
    }
}

VariantVector TreeLoader::GetTreesAttr() const
{
    VariantVector ret;
    ret.Reserve(trees_.Size() * 4 + 1);
    ret.Push(trees_.Size());

    for (PODVector<TreeInfo>::ConstIterator i = trees_.Begin(); i != trees_.End(); ++i)
    {
        ret.Push(i->color_);
        ret.Push(i->position_);
        ret.Push(i->rotation_);
        ret.Push(i->scale_);
    }

    return ret;
}

} // Foliage

} // Urho3D
