#pragma once

#include "Foliage/PatchLayer.h"
#include "Foliage/PatchLoader.h"

namespace Urho3D
{

class BillboardSet;
class Material;
class Terrain;
class XMLFile;

namespace Foliage
{

class BillboardMap;
class TreeLoader;

struct TreeInfo
{
    Color color_;
    Vector2 position_;
    float rotation_;
    Vector2 scale_;
};

class URHO3D_API Tree : public Component
{
URHO3D_OBJECT(Tree, Component);

public:
    /// Construct.
    Tree(Context *context);

    /// Construct.
    Tree(Context *context, unsigned index, const TreeInfo &info, TreeLoader *loader);

    /// Destruct.
    ~Tree();

    unsigned GetIndex() const { return index_; };
    const TreeInfo& GetInfo() const { return info_; }
    TreeLoader *GetLoader() const { return loader_; }

private:
    /// Index in the loaders treeInfo_ vector.
    unsigned index_;

    /// Copy of the tree info which was used to generate this instance.
    /// Changing these values has no effect
    TreeInfo info_;

    /// Loader which was used to load this instance.
    WeakPtr<TreeLoader> loader_;
};

class URHO3D_API TreeLayer : public PatchLayer
{
friend class TreeLoader;

public:
    TreeLayer(TreeLoader *loader);

    virtual void UpdateLodDistance(float newDistance, Patch* patch);
    virtual PatchLoader* GetLoader() const;

private:
    TreeLoader *loader_;

    Vector3 oldDir;

    bool modelsLoaded_;
    bool billboardsLoaded_; 

    WeakPtr<BillboardSet> billboards_;
    Vector<WeakPtr<Node> > models_;
};

class URHO3D_API TreeLoader : public PatchLoader
{
URHO3D_OBJECT(TreeLoader, PatchLoader);

public:
    friend class TreeLayer;

    /// Construct
    TreeLoader(Context *context);

    /// Destruct
    ~TreeLoader();

    /// Register object factory
    static void RegisterObject(Context* context);

    /// Called when PatchManager wants us to generate a new patch
    virtual void Load(Patch* patch);

    /// Called every frame
    virtual void Update(PatchManager *pm, float timeStep);
    
    /// Add a new tree instance.
    /// Returns true if successfull.
    bool AddInstance(const TreeInfo& instance);

    /// Add a new tree instance.
    /// Returns true if successfull.
    bool AddInstanceHelper(const Vector2& position, float rotation, const Vector2& scale);

    /// Remove a tree instance by given index.
    void RemoveInstance(unsigned index);

    /// Remove all instances.
    void RemoveInstances();

    /// Sets the tree instance prefab.
    void SetPrefab(XMLFile *file);

    void SetCreateComponents(bool value) { createTreeComponents_ = value; }
    void SetBillboardDistance(float value) { billboardDistance_ = value; }
    void SetBillboardFadeDistance(float value) { billboardFadeDistance_ = value; }

    /// Sets whether not this TreeLoader shoudl
    void SetCreateTreeComponents(bool value);

    /// Return the tree instance prefab
    XMLFile* GetPrefab() { return treePrefab_; }

    bool GetCreateComponents() const { return createTreeComponents_; }
    float GetBillboardDistance() const { return billboardDistance_; }
    float GetBillboardFadeDistance() const { return billboardFadeDistance_; }

    void SetTreePrefabAttr(const ResourceRef& value);
    ResourceRef GetTreePrefabAttr() const;

    void SetTreesAttr(const VariantVector& value);
    VariantVector GetTreesAttr() const;

private:
    void GetTreeIndices(PODVector<unsigned>& values, const Rect& bounds) const;

    void LoadBillboards(Patch *patch, TreeLayer *layer);
    void LoadModels(Patch *patch, TreeLayer *layer);

    void UpdateMaterial(Material* material);

    /// Returns the patch where the position belongs to
    /// If the position is outside of terrain bounds, NULL is returned.
    //  return;
    Vector<unsigned>* GetPatch(const Vector2& position);

    /// Returns patch by index, if patch is out of bounds, NULL is returned.
    Vector<unsigned>* GetPatch(const IntVector2& index);

    /// Information about every tree contained in this loader.
    PODVector<TreeInfo> trees_;

    /// Prefab used to instantiate tree instances
    SharedPtr<XMLFile> treePrefab_;

    /// Generated billboard map for this tree type
    SharedPtr<BillboardMap> billboardMap_;

    /// At what distance we should switch to billboard rendering. 
    float billboardDistance_;

    /// Fade distance between billboards and models.
    float billboardFadeDistance_;

    /// Should this loader add Tree component to generated tree instances?
    bool createTreeComponents_;

};

} // Foliage

} // Urho3D
