#pragma once

#include <Urho3D/Scene/Component.h>

namespace Urho3D
{

class Terrain;

namespace Foliage
{

class Patch;
class PatchLoader;

class URHO3D_API PatchManager : public Component
{
URHO3D_OBJECT(PatchManager, Component);

public:
    friend class Patch;

    /// Construct
    PatchManager(Context *context);
    /// Destruct
    ~PatchManager();

    static void RegisterObject(Context *context);

    virtual void OnSetAttribute(const AttributeInfo& attr, const Variant& src);
        
    /// Visualize the component as debug geometry.
    virtual void DrawDebugGeometry(DebugRenderer* debug, bool depthTest);

    /// Visualize the component as debug geometry.
    void DrawDebugGeometry(bool depthTest);

    void AddLoader(PatchLoader *loader);
    PatchLoader* GetLoader(unsigned index);
    unsigned GetNumLoaders() const;
    void RemoveLoader(unsigned index);
    void RemoveLoader(PatchLoader *loader);

    void SetPatchSubvisions(int value);
    void SetMaxLoadTime(unsigned ms) { maxLoadTime_ = ms; }
    void SetUpdateInterval(float s) { updateInterval_ = s; }
    void SetTerrain(Terrain *terrain);
    void SetCamera(Node *value);
    void SetCameraNodeIdAttr(unsigned value);
    void SetViewPosition(const Vector3 &value) { viewPos_ = value; }
    void SetLoadDistance(float value) { loadDistance_ = value; }

    int GetPatchSubdivisions() const { return patchSubdivisons_; }
    unsigned GetMaxLoadTime() const { return maxLoadTime_; }
    Terrain* GetTerrain() const { return terrain_; }
    unsigned GetCameraNodeIdAttr() const;
    Vector3 GetViewPosition() const { return viewPos_; }
    float GetLoadDistance() const { return loadDistance_; }

    Vector2 GetPatchSize() const { return patchWorldSize_; }

    Vector2 GetSize() const;
    Rect GetBounds() const;


private:
    void Start();
    void OnUpdate(StringHash eventType, VariantMap& eventData);
    void OnTerrainCreated(StringHash eventType, VariantMap& eventData);

    void LoadArea(const Vector3& point, float distance);
    void LoadArea(const Rect& area);
    void LoadPatchRegion(const IntRect& area);
    void LoadPatch(Scene* scene, int x, int z);

    void ReloadPatch(Patch* patch);
    void ReloadAllPatches();

    void RemovePatch(Patch *patch);
    void RemovePatches();

    Patch* GetPatch(int x, int z);

    float loadDistance_;
    float cacheDistance_;

    int patchSubdivisons_;

    /// How many milliseconds to spend loading patches.
    unsigned maxLoadTime_;

    /// How often to perform updates.
    float updateInterval_;

    /// Terrain we use to position the foliage.
    WeakPtr<Terrain> terrain_;

    /// Total size of the patch grid.
    IntVector2 patchesSize_;

    /// Size of the patch in world space
    Vector2 patchWorldSize_;

    /// 2D array of patches acccessed using patchesSize_.y_ * y + x.
    Vector<WeakPtr<Patch> > patches_;

    /// Every patch loader registed to this PatchManager.
    /// The patches are stored using shared ptrs so that we can safely unload them.
    Vector<SharedPtr<PatchLoader> > loaders_;

    /// Contains every patch that has been loaded.
    Vector<SharedPtr<Patch> > loadedPatches_;

    /// If not null, viewPos_ is set every update to the camera_'s position
    WeakPtr<Node> camera_;

    /// Point in world space used to determine which patches to load/unload
    Vector3 viewPos_;

    /// Used to time interval between patch updates.
    float updateTimer_;

    /// Has the Start() method been called? 
    bool startCalled_;

    /// Set to true if all patches should be reloaded.
    /// This flag is mostly set when attributes of the PatchManager
    /// which require full reload are modified.
    bool recreate_;
};

} // Foliage

} // Urho3D
