
- Add individual load distances to loaders

- Add option to manuall update X milliseconds.
  return true if nothing to do

- Add update interval parameter to patch manager
- Add max update duration to patch manager
- Generate tree colliders

- Update patch manager to pool patch nodes
    - Not urgent right now, most of the time is spent rendering anyway
    - Little effect for the memory

- Update tree loader to support sphere wind
- Update tree loader to remove nodes after a while, when rendering billboards

- Move yOffset along normal

- Finish the billboard map generator

    - Fix billboards in the wind
    - Serialize the billboards
        - BillboardMap data could be stored as StringHash(ResourceName).billboard.xml in some folder

- Implement proper noise function
- Implement Lua and Angelscript API

- Come up with a better name for the project
- Test on windows

- Get proper assets
    - Put in a folder SourceAssets

- List also the asset contributors in the authors or somewhere
- Update kyvyt.fi blog

- Make Discourse account
    - Announce release

- Go throught the code and reorder functions according to contribution guide
- Go throught the code and encapsulate few things better, loaders for example
- Write documentation

- DONE 2017-08-04: Remove Normal influence from tree loaders, as it wont work with billboards

- DONE 2017-08-03: Calculate the proper view to display
- DONE 2017-08-03: Calculate the correct size for the billboard

- DONE: Implement samples 1-2 
    - DONE: 1 could be just a small patch with grass
    - DONE: 2 could be heightmapped and multiple loaders

- DONE: Write licence

- DONE: Remove unused files

- DONE: Handle foliage loader registration better
    - DONE: Should not depend on component order

- DONE: Handle attribute changes
    - Update only affected layers

- DONE: Update loaders to support default Drawable attributes, shadowMask, lightMask, etc.
    - DONE: Make them actually do their effect

- DONE: Update tree loader to support billboard shadows

- DONE 2017-07-28: Seed randomly by patch

- DONE 2017-07-27: Allow removing loaders

- DONE 2017-07-27: Make random tilt to actually do something
- DONE 2017-07-27: Make normal influence to actually do something
- DONE: Change size attributes to use min-max values
- DONE: Write authors.txt

- DONE 2017-07-31: Update only loaded patches
